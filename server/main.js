import { Meteor } from 'meteor/meteor';
import { meteor_id, ProductsCollection } from '../imports/methods/links';
import { Mongo } from 'meteor/mongo';
import { Accounts } from 'meteor/accounts-base';
import '../imports/methods/ProductsMethods'
import '../imports/methods/UserRegisterMethods'
import '../imports/methods/UserCartMethods'
import '../imports/methods/publication'
import '../imports/methods/PaymentMethods'
import '../imports/methods/PackagesMethods'

Meteor.startup(() => {

});