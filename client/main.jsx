import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import { App } from '/imports/ui/App';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { store } from '../imports/store/store';
import 'react-notifications-component/dist/theme.css'
import 'react-modern-calendar-datepicker/lib/DatePicker.css';

Meteor.startup(() => {
  render(<Provider store={store}>
    <BrowserRouter><App /></BrowserRouter>
  </Provider>, document.getElementById('react-target'));
});
