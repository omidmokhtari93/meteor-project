import { getCartCount } from '../store/action-creators';
import { store } from '../store/store'

export const cart = {
    get: () => JSON.parse(localStorage.getItem('irancell-cart')),
    add: function (item) {
        const oldCart = this.get();
        if (oldCart) {
            this.mergeCart(oldCart.products, item)
        } else {
            this.set({
                itemsTotal: parseInt(item.price),
                tax: 7200,
                total: 7200 + parseInt(item.price),
                discount: 0,
                products: [item]
            })
        }
    },
    set: (item) => {
        const total = item.products.length && item.products.map(x => parseInt(x.price) * x.quantity)
            .reduce((prev, next) => parseInt(prev) + parseInt(next));
        const updatedCart = {
            ...item,
            total: 7200 + total,
            itemsTotal: total
        }
        localStorage.setItem('irancell-cart', JSON.stringify(updatedCart))
        store.dispatch(getCartCount())
    },
    mergeCart: function (oldCart, newItem) {
        const cartObject = this.get()
        let updatedCart = oldCart;
        const index = oldCart.findIndex((x) => x.productId == newItem.productId &&
            x.package._id == newItem.package._id)
        if (index != -1) {
            updatedCart[index].quantity += 1;
        } else {
            updatedCart = [...oldCart, newItem]
        }
        this.set({ ...cartObject, products: updatedCart })
    },
    updateQuantity: function (productId, packageId, value) {
        let items = this.get();
        items.products.map((x, idx) => {
            if (x.productId == productId &&
                x.package._id == packageId) {
                if (value > 0) {
                    items.products[idx].quantity += value;
                } else {
                    items.products[idx].quantity > 1 ?
                        (items.products[idx].quantity += value) : null
                }
            }
        })
        this.set(items)
        return this.get();
    },
    removeOneItem: function (productId, packageId) {
        let items = this.get();
        const filtered = items.products.filter(x => !(x.productId == productId && x.package._id == packageId))
        this.set({ ...items, products: filtered })
        return this.get()
    },
    remove: () => localStorage.removeItem('irancell-cart'),
    count: function () {
        const cartObject = this.get()
        return cartObject && cartObject.products.length
    }
}