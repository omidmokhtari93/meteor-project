
export const CountDownTimer = {
    timer: null,
    subscribe: function (dt, callback) {
        var end = new Date(dt);
        var _second = 1000;
        var _minute = _second * 60;
        var _hour = _minute * 60;
        var _day = _hour * 24;

        function showRemaining() {
            var now = new Date();
            var distance = end - now;
            if (distance < 0) {

                clearInterval(this.timer);
                callback('EXPIRED !!!')
                return;
            }
            var days = Math.floor(distance / _day);
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);
            var seconds = Math.floor((distance % _minute) / _second);
            callback({ days, hours, minutes, seconds })
        }

        this.timer = setInterval(showRemaining, 1000);
    },
    unsubscribe: function () {
        clearInterval(this.timer)
    }
}