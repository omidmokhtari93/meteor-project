import { store } from 'react-notifications-component';

export const notify = (message, type) => {
    store.addNotification({
        message: message,
        type: type,
        insert: "top",
        container: "top-left",
        dismiss: {
            duration: 1000,
            onScreen: true
        }
    });
}