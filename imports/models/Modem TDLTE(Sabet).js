for (let index = 1; index < 4; index++) {
    ProductsCollection.insert({
        category: 'fixed_modem',
        title: `مودم TDLTE - TK251${index} Plus با بسته اینترنت انتخابی`,
        description: [
            'بسته اولیه با انتخاب شما',
            'قابلیت اتصال همزمان تا 30 نفر از طریق WI-FI',
            'بیشینه سرعت دانلود تا 120Mbps',
            'قابلیت اتصال به اینترنت از طریق کابل LAN'
        ],
        features: [
            { title: 'نوع مودم', description: 'داخلی' },
            { title: 'ابعاد', description: `233*45*${index * 10} mm` },
            { title: 'وزن', description: `گرم ${index * 100}` },
            { title: 'شبکه های قابل پشتیبانی', description: 'TDD only' },
            { title: 'باند های قابل پشتیبانی', description: `Band 4${index}(500MHz) ,Band43(350MHz)` },
            { title: 'پشتیبانی از MIMO', description: 'دارد' },
            { title: 'Mono WI-FI', description: 'ندارد' },
            { title: 'منبع تغذیه', description: 'دارد' },
            { title: 'چیپست', description: `Snapdragon 4.${index}` },
        ],
        images: [
            '/img/modem.jpg',
            '/img/modem1.jpg',
            '/img/modem1.jpg',
            '/img/modem.jpg',
        ],
        packages: [
            { _id: new Mongo.ObjectID()._str, type: 'prepaid', name: `بسته ${index * 20} گیگابایت یکساله`, amount: index * 20, price: '60,000', oldPrice: '67,000', available: true },
            { _id: new Mongo.ObjectID()._str, type: 'prepaid', name: `بسته ${index * 20 + 5} گیگابایت یکساله`, amount: index * 20 + 5, price: '75,000', oldPrice: '', available: true },
            { _id: new Mongo.ObjectID()._str, type: 'prepaid', name: `بسته ${index * 20 + 10} گیگابایت یکساله`, amount: index * 20 + 10, price: '85,000', oldPrice: '', available: true },
            { _id: new Mongo.ObjectID()._str, type: 'prepaid', name: `بسته ${index * 20 + 15} گیگابایت یکساله`, amount: index * 20 + 15, price: '85,000', oldPrice: '', available: true },
            { _id: new Mongo.ObjectID()._str, type: 'postpaid', name: `بسته ${index * 20 + 5} گیگابایت یکساله`, amount: index * 20 + 5, price: '60,000', oldPrice: '', available: true },
            { _id: new Mongo.ObjectID()._str, type: 'postpaid', name: `بسته ${index * 20 + 10} گیگابایت یکساله`, amount: index * 20 + 10, price: '75,000', oldPrice: '80,000', available: true },
            { _id: new Mongo.ObjectID()._str, type: 'postpaid', name: `بسته ${index * 20 + 15} گیگابایت یکساله`, amount: index * 20 + 15, price: '85,000', oldPrice: '', available: true },
        ],
        networkCoverage: {},
        setting: {},
        available: index % 2 == 0,
        price: index * 120 + '000',
        oldPrice: index % 2 == 0 ? (index * 100 - 50) + '000' : ''
    })
}