import { ProductsCollection } from "../methods/links"

const PackagesModel = {
    category: 'TDLTE_PREPAID', // TDLTE_POSTPAID , GSM_PREPAID , GSM_POSTPAID
    validity: 'Hourly',  //Hourly ,Daily ,3-Day ,Weekly ,15-Day ,20-Day ,Monthly ,2-Months ,3-Months ,4-Months ,6-Months ,Yearly
    trafficLimit: 1000, // MB
    title: '10 گیگ + 1 گیگ (6 صبح تا 12 ظهر)',
    ussdCode: '*555*5*6*4#',
    price: 55000
}

let cats = ['TDLTE_PREPAID', 'TDLTE_POSTPAID', 'GSM_PREPAID', 'GSM_POSTPAID']
let validity = ['Hourly', 'Daily', '3-Day', 'Weekly', '15-Day', '20-Day', 'Monthly', '2-Months', '3-Months', '4-Months', '6-Months', 'Yearly']
for (let i = 0; i < cats.length; i++) {
    for (let z = 0; z < validity.length; z++) {
        for (let j = 0; j < 7; j++) {
            ProductsCollection.insert({
                category: cats[i],
                networCoverage: '2G, 3G, 4G, 4.5G',
                validity: validity[z],
                trafficLimit: 1000 * (j + 1),
                title: `${j + 1} گیگ + 1 گیگ (6 صبح تا 12 ظهر)`,
                ussdCode: `*555*5*6*${j}#`,
                price: 2000 * (j + 1)
            })
        }
    }
}