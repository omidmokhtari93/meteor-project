import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { UserCartCollection } from './links'

Meteor.publish('cart.get', function() {
    if (!this.userId) {
        throw new Meteor.Error('error on getting cart count')
    }

    return UserCartCollection.find({ userId: this.userId, status: 'active' }, { fields: { userId: 0 } })
})