import { check } from 'meteor/check';
import { Accounts } from 'meteor/accounts-base';
import { ProvinceCityCollection, UserCartCollection, UserDataCollection } from './links';
import { Mongo } from 'meteor/mongo';

const checkExistAddress = (existAddress, newAddress) => {
    const isExist = existAddress.map(x => {
        return x.postalCode == newAddress.postalCode &&
            x.city == newAddress.city &&
            x.province == newAddress.province
    }).includes(true)
    return isExist;
}

Meteor.methods({
    'user.create' (username, password) {
        check(username, String)
        check(password, String)
        if (!Accounts.findUserByUsername(username)) {
            Accounts.createUser({
                username: username,
                password: password
            })
            Meteor.users.update({ username: username }, { $set: { credit: 20000 } })
            return { type: 'success', message: 'حساب ایجاد شد' }
        } else {
            return { type: 'danger', message: 'این نام کاربری از قبل وجود دارد' }
        }
    },
    'user.info.get' () {
        if (!this.userId) {
            throw new Meteor.error('خطا در دریافت اطلاعات کاربر')
        }
        return {
            userData: UserDataCollection.find({ userId: this.userId }).fetch(),
            provinceAndCities: ProvinceCityCollection.find({}).fetch()
        }
    },
    'user.getCredit.cartTotal' () {
        const user = Meteor.users.findOne({ _id: this.userId })
        const cart = UserCartCollection.findOne({ userId: this.userId, status: 'active' })
        return { credit: user.credit, cartTotal: cart.total }
    },
    'user.personalinfo.save' (state) {
        check(state, Object)
        if (!this.userId) {
            throw new Meteor.error('خطا در ثبت اطلاعات کاربر')
        }
        if (UserDataCollection.find({ userId: this.userId }).count()) {
            UserDataCollection.update({ userId: this.userId }, { $set: state })
            return
        }
        UserDataCollection.insert({
            ...state,
            userId: this.userId
        })
    },
    'user.contactinfo.save' (state) {
        if (!this.userId) {
            throw new Meteor.error('خطا در دریافت اطلاعات کاربر')
        }
        const addressId = new Mongo.ObjectID()._str;
        const item = UserDataCollection.findOne({ userId: this.userId }, { fields: { deliveryAddress: 1, _id: 0 } })
        UserDataCollection.update({ userId: this.userId }, {
            $set: {
                ...state,
                ...(!item.deliveryAddress && {
                    deliveryAddress: [{
                        ...state,
                        _id: addressId
                    }]
                })
            }
        })
        const cartDeliveryAddress = UserCartCollection.findOne({ userId: this.userId, status: 'active' }, {
            fields: { deliveryAddress: 1, _id: 0 }
        })
        if (!cartDeliveryAddress.deliveryAddress) {
            UserCartCollection.update({ userId: this.userId, status: 'active' }, {
                $set: {
                    deliveryAddress: {
                        ...item.deliveryAddress ? item.deliveryAddress[0] : {...state, _id: addressId }
                    }
                }
            })
        }

    },
    'user.get.address' () {
        if (!this.userId) {
            throw new Meteor.error('خطا در دریافت آدرس کاربر')
        }
        const addresses = UserDataCollection.findOne({ userId: this.userId }, { fields: { deliveryAddress: 1, _id: 0 } })
        const address = UserCartCollection.findOne({ userId: this.userId, status: 'active' }, { fields: { deliveryAddress: 1, _id: 0 } })
        return {
            addresses: addresses.deliveryAddress,
            deliveryAddress: address.deliveryAddress
        }
    },
    'user.addOrEdit.address' (action, newAddress) {
        if (!this.userId) {
            throw new Meteor.error('خطا در ثبت آدرس کاربر')
        }
        const addresses = UserDataCollection.findOne({ userId: this.userId }, { fields: { deliveryAddress: 1, _id: 0 } })
        if (action == 'add') {
            const isAdded = checkExistAddress(addresses.deliveryAddress, newAddress)
            if (isAdded) {
                return { message: 'این آدرس قبلا ثبت شده است', type: 'danger' }
            }
            UserDataCollection.update({ userId: this.userId }, {
                $set: {
                    deliveryAddress: [
                        ...addresses.deliveryAddress,
                        {...newAddress, _id: new Mongo.ObjectID()._str }
                    ]
                }
            })
            return { message: 'آدرس ثبت شد', type: 'success' }
        }
        let updatedAddresses = addresses.deliveryAddress
        addresses.deliveryAddress.map((x, idx) => {
            x._id == newAddress._id && (updatedAddresses[idx] = newAddress)
        })
        UserDataCollection.update({ userId: this.userId }, { $set: { deliveryAddress: updatedAddresses } })
        return { message: 'آدرس بروزرسانی شد', type: 'success' }

    },
    'user.update.deliveryAddres' (selectedAddress) {
        if (!this.userId) {
            throw new Meteor.error('خطا در بروزرسانی آدرس دریافت محصول')
        }
        UserCartCollection.update({ userId: this.userId, status: 'active' }, {
            $set: {
                deliveryAddress: selectedAddress
            }
        })
        return { message: 'آدرس دریافت بروزرسانی شد', type: 'success' }
    }
})