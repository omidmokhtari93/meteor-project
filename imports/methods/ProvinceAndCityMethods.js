import { ProvinceCityCollection } from '../methods/links'

Meteor.methods({
    'province.city.get'() {
        if (!this.userId) {
            throw new Meteor.erorr('خطا در دریافت اطلاعات شهر و استان')
        }
        return ProvinceCityCollection.find().fetch()
    }
})