import { Mongo } from 'meteor/mongo';

export const ProductsCollection = new Mongo.Collection('products')
export const UserCartCollection = new Mongo.Collection('shopping_cart')
export const ProvinceCityCollection = new Mongo.Collection('city_province')
export const UserDataCollection = new Mongo.Collection('user_information')
export const meteor_id = new Mongo.ObjectID()._str;