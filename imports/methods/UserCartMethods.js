import { UserCartCollection } from './links'
import { check } from 'meteor/check';

const updateCart = (item) => {
    const total = item.products.length && item.products.map(x => parseInt(x.price) * x.quantity)
        .reduce((prev, next) => parseInt(prev) + parseInt(next));
    return {
        ...item,
        total: 7200 + total,
        itemsTotal: total
    }
}

const findExists = (existItem, newItem) => {
    const index = existItem.products.findIndex(x => {
        return x.productId == newItem.productId &&
            x.package._id == newItem.package._id
    })
    if (index != -1) {
        existItem.products[index].quantity += 1;
    } else {
        existItem.products = [...existItem.products, newItem]
    }
    return updateCart(existItem)
}
const updateQuantity = (cart, productId, packageId, value) => {
    const index = cart.products.findIndex(x => {
        return x.productId == productId &&
            x.package._id == packageId
    })
    if (index != -1) {
        if (cart.products[index].quantity > 1 && value < 0 || value > 0) {
            cart.products[index].quantity += value
        }
    }
    return updateCart(cart)
}

Meteor.methods({
    'cart.add' (selectedProduct) {
        if (!this.userId) {
            throw new Meteor.Error('کاربر پیدا نشد');
        }
        check(selectedProduct, Object)
        const message = { type: 'success', message: 'به سبد خرید اضافه شد' }
        const existItem = UserCartCollection.findOne({ userId: this.userId, status: 'active' });
        if (existItem) {
            const updatedCart = findExists(existItem, selectedProduct)
            UserCartCollection.update({ userId: this.userId, status: 'active' }, { $set: updatedCart })
            return message
        }

        UserCartCollection.insert({
            userId: this.userId,
            itemsTotal: parseInt(selectedProduct.price),
            tax: 7200,
            total: 7200 + parseInt(selectedProduct.price),
            discount: 0,
            status: 'active',
            products: [
                selectedProduct
            ]
        })
        return message
    },
    'cart.get' () {
        if (!this.userId) {
            throw new Meteor.Error('سبد پیدا نشد');
        }
        return UserCartCollection.findOne({
            userId: this.userId,
            status: 'active'
        }, {
            fields: { userId: 0, status: 0 }
        })
    },
    'cart.merge' (cartItems) {
        if (!this.userId) {
            throw new Meteor.Error('خطا در ثبت');
        }
        check(cartItems, Object);
        const response = { type: 'success', message: 'سبد خرید ذخیره شد' };

        const existCart = UserCartCollection.findOne({ userId: this.userId, status: 'active' });
        if (existCart) {
            let products = existCart.products;
            cartItems.products.map(item => {
                const index = products.findIndex(x => x.productId == item.productId && x.package._id == item.package._id)
                if (index != -1) {
                    products[index].quantity += item.quantity
                } else {
                    products = [...products, item]
                }
            })
            const updatedCart = updateCart({
                ...existCart,
                products: products
            })
            UserCartCollection.update({ userId: this.userId, status: 'active' }, { $set: updatedCart })
            return response
        }
        UserCartCollection.insert({
            ...cartItems,
            userId: this.userId,
            status: 'active'
        })
        return response
    },
    'cart.quantity' (productId, packageId, value) {
        if (!this.userId) {
            throw new Meteor.Error('خطا');
        }
        check(productId, String)
        check(packageId, String)
        check(value, Number)
        const pr = UserCartCollection.findOne({ userId: this.userId, status: 'active' })
        UserCartCollection.update({ userId: this.userId, status: 'active' }, {
            $set: updateQuantity(pr, productId, packageId, value)
        })
        return UserCartCollection.findOne({ userId: this.userId, status: 'active' })
    },
    'cart.removeOneItem' (productId, packageId) {
        check(productId, String)
        check(packageId, String)
        if (!this.userId) {
            throw new Meteor.Error('خطا');
        }
        const cart = UserCartCollection.findOne({ userId: this.userId, status: 'active' })
        const filtered = cart.products.filter(x => !(x.productId == productId && x.package._id == packageId))
        const updatedCart = updateCart({
            ...cart,
            products: filtered
        })
        UserCartCollection.update({ userId: this.userId, status: 'active' }, { $set: updatedCart })
        return UserCartCollection.findOne({ userId: this.userId, status: 'active' })
    },
    'cart.count' () {
        if (!this.userId) {
            throw new Meteor.Error('خطا');
        }
        return UserCartCollection.findOne({ userId: this.userId, status: 'active' })
    }
})