import { ProductsCollection } from './links'
import { check } from 'meteor/check';

Meteor.methods({
    'packages.get'({ category, simType, validity, trafficLimit }) {
        check(category, String)
        check(simType, String)
        check(validity, Array)
        check(trafficLimit, Array)
        const validities = { $or: validity.map(x => ({ validity: x })) }
        const greaterThan10G = trafficLimit.includes(100000) && trafficLimit.length == 1
        const trafficLimits = {
            $or: greaterThan10G
                ? trafficLimit.map(x => ({
                    trafficLimit: { $gt: x }
                }))
                : trafficLimit.map(x => ({
                    trafficLimit: { $lte: x }
                }))
        }
        //console.log(validities, trafficLimits, { category: `${category.toUpperCase()}_${simType}` })
        return ProductsCollection.find({
            $and: [
                { category: `${category.toUpperCase()}_${simType}` },
                validities,
                trafficLimits
            ]
        }).fetch()
    },
    'packages.getOne'(id) {
        check(id, String)
        return ProductsCollection.findOne({ _id: id })
    }
})