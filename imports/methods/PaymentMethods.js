import { UserCartCollection, UserDataCollection } from "./links"

Meteor.methods({
    'payment' (method) {
        if (!this.userId) {
            throw new Meteor.error('خطا در پرداخت')
        }
        let registrationData = UserDataCollection.findOne({ userId: this.userId })
        let cart = UserCartCollection.findOne({ userId: this.userId, status: 'active' })
        cart.paymentMethod = method;
        cart.status = 'paid'
        cart.products.map((x, idx) => {
            cart.products[idx].registrationData = registrationData;
        })
        UserCartCollection.update({ userId: this.userId, status: 'active' }, { $set: cart })
        return { message: 'با موفقیت پرداخت شد', type: 'success' }
    }
})