import { check } from 'meteor/check';
import { ProductsCollection } from './links'

Meteor.methods({
    'products.getByCategory' (category) {
        check(category, String)
        const searchObject = category == 'modem' ? [{ category: 'fixed_modem' }, { category: 'portable_modem' }] :
            category == 'simcard' ? [{ category: 'sim_prepaid' }, { category: 'sim_postpaid' }] : [];
        const result = ProductsCollection.find({
            $or: searchObject
        }).fetch()
        if (result) {
            return result
        }
    },
    'product.GetDetailes' (productId) {
        check(productId, String);
        return ProductsCollection.findOne({ _id: productId })
    },
    'product.getCartItems' (cartItems) {
        check(cartItems, Array)
        const pr = cartItems.map(x => ({ "$and": [{ _id: x.productId }, { 'packages._id': x.packageId }] }))
        return ProductsCollection.find({
            "$or": pr
        }).fetch()
    }
})