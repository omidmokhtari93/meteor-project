import { createStore } from "redux";
import { reducers } from "./combine-reducers";

export const store = createStore(reducers)