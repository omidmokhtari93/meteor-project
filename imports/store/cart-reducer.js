import { GET_CART_COUNT } from "./actions";

const initState = {
    cartCount: 0
}

export const CartReducer = (state = initState, action) => {
    const cart = JSON.parse(localStorage.getItem('irancell-cart'));
    return cart && cart.products ? cart.products.length : 0;
}