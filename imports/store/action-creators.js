import { GET_CART_COUNT } from './actions'

export const getCartCount = () => {
    return {
        type: GET_CART_COUNT,
    }
}