import { combineReducers } from "redux";
import { CartReducer } from "./cart-reducer";


export const reducers = combineReducers({
    cart: CartReducer
})