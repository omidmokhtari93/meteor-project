import React from 'react'


export const Tabs = props => {
    //console.log(props)
    return (
        <div className={`tabs nav nav-tabs radius ${props.className ? props.className : ''}`}>
            {props.tabs && props.tabs.map(x => <a key={x.command}
                className={`nav-item nav-link border-left py-3 px-4 ${x.command == props.selected && 'active'}`}
                onClick={() => props.toggleTabs(x.command)}>
                {x.text}
            </a>)}
        </div>
    )
}