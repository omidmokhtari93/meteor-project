import React from 'react'


export const Button = props => {
    return (
        <button className={`noselect sans btn-irancell ${props.color ? props.color : 'yellow'}
            ${props.className ? props.className : ''}`}
            disabled={props.disabled ? props.disabled : false}
            onClick={() => props.onClick && props.onClick()}
            style={{ height: props.height || '40px' }}>
            {props.children}
        </button>
    )
}