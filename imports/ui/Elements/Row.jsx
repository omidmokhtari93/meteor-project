import React from 'react'



export const Row = props => {
    //console.log(props.className)
    const col = 12 / (props.children.length ? props.children.length : 1)
    return (
        <div className={`row sans ${props.className ? props.className : 'mb-3'}`}>
            {[...Array(props.children.length)].map((x, idx) => {
                return <div key={idx}
                    className={`${props.childClass ? props.childClass : ''} col-${props.type ? props.type : 'md'}-${col}`}>
                    {props.children.length ? props.children[idx] : props.children}
                </div>
            })}
        </div>
    )
}