import React from 'react'



export const RoundedRadioButton = props => {

    return <div className="row m-0 rounded-radio-button">
        {props.options && props.options.map(x => {
            return <div className={`col-${props.col || '4'} ${props.colClassName || ''} noselect`}
                key={x.id} onClick={() => props.handleChange(x.id)}>
                <label className={`d-flex align-content-center font-size-${props.fontSize || '11'} 
                                    box ${props.multiple
                        ? props.selected.includes(x.id) ? 'active' : ''
                        : props.selected == x.id ? 'active' : ''}`}
                    style={props.style || {}}>
                    <span className={`font-weight-800 m-auto price font-size-${props.fontSize || '12'}`}>{x.text}</span> {x.unit}
                </label>
            </div>
        })}
    </div>
}