import React from 'react'


export const Collapse = props => {
    return <div className={`card p-3 p-lg-4 sans rounded-m app-shadow irancell-collapse ${props.className || ''}`}>
        <div className="row d-flex align-items-center"
            onClick={() => props.handleChange(!props.collapsed)}>
            <div className="col-12 col-md-11 col-lg-11 text-right">
                <p className={`font-weight-800 ${props.description || 'mb-0'}`}>{props.title}</p>
                <p className="mb-0 font-size-13 color-dark">
                    {props.description}
                </p>
            </div>
            <div className="col-12 col-md-1 col-lg-1 ltr">
                <label className={`fa fa-angle-${props.collapsed ? 'up' : 'down'} 
                mt-2 mt-lg-0 ml-auto mr-auto ml-lg-0 d-block dark-irancell`}></label>
            </div>
        </div>
        <div className={`collapsed ${props.collapsed ? 'active' : ''}`}>
            {props.children}
        </div>
    </div>
}