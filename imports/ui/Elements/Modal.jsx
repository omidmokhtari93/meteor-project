import React from 'react'
import { Backdrop } from './Backdrop'


export const Modal = props => {
    return <React.Fragment>
        <div className={`sans dark-irancell irancell-modal card ${props.className ? props.className : ''}`}
            style={{ top: props.show ? '10%' : '-1000px' }}>
            {props.children}
        </div>
        {props.show && <Backdrop onClick={props.closeModal} />}
    </React.Fragment>
}