import React from 'react'



export const DarkInput = props => {
    return <div className={`dark-input noselect ${props.className || ''}`}>
        {props.label && <label>{props.label}</label>}
        <input
            style={{ height: props.height || '40px' }}
            type={props.type || 'text'}
            value={props.value}
            disabled={props.disabled || false}
            placeholder={props.placeholder || ''}
            onChange={e => props.handleChange(e.target.value)}
        />
    </div>
}