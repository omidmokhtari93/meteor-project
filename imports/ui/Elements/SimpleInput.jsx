import React from 'react'



export const SimpleInput = props => {
    return <input
        type={props.type || 'text'}
        className={`simple-input form-control font-size-14 ${props.className || ''}`}
        placeholder={props.placeholder || ''}
        style={{ height: props.height || '40px' }}
    />
}