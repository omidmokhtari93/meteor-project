import React, { useEffect, useState } from 'react'
import DatePicker from 'react-modern-calendar-datepicker';


export const DatePickerContainer = props => {
    //console.log(props)
    const [labelStyle, setLabelStyle] = useState('')
    useEffect(() => {
        props.value && setLabelStyle('active')
    }, [props.value])
    const addStyles = e => {
        if (labelStyle == '') {
            setLabelStyle('active')
        }
    }

    const onBlurInput = () => {
        if (!props.value) {
            setLabelStyle('')
        }
        props.handleChange(props.name, props.value)
    }
    const isInvalid = props.required && props.touched && !props.value
    return (
        <React.Fragment>
            <div className={`irancell-datepicker app-shadow ${isInvalid ? 'invalid' : ''}`}
                onClick={addStyles}
                onBlur={() => onBlurInput()}>
                <DatePicker
                    value={props.value}
                    onChange={(e) => props.handleChange(props.name, e)}
                    locale="fa"
                />
                <label className={`noselect ${labelStyle}`}>{props.label}</label>
                {isInvalid && <span className="fa fa-exclamation-circle"></span>}
            </div>
            {props.message && isInvalid &&
                <p className="irancell-datepicker-message mb-0 mt-1 text-right ltr">{props.message}</p>}
        </React.Fragment >
    )
}