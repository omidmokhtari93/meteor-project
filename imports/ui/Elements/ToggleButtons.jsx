import React from 'react'



export const ToggleButtons = props => {
    return <div className={`irancell-toggle-butons ${props.className || ''}`}>
        <div className="list-inline">
            {props.options.map(x => <div
                className={`list-inline-item ${x.id == props.selected ? 'active' : ''}`}
                onClick={() => props.handleChange(x.id)}
                key={x.id}>{x.text}</div>)}
        </div>
    </div>
}