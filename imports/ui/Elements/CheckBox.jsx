import React from 'react'



export const CheckBox = props => {
    return (
        <React.Fragment>
            <div className={`custom-control custom-checkbox ${props.className ? props.className : ''}`}>
                <input type="checkbox" className="custom-control-input" id={props.id} checked={props.checked || false}
                    onChange={() => props.handleChange()} />
                <label className={`custom-control-label noselect ${props.labelClass ? props.labelClass : ''}`}
                    htmlFor={props.id}>{props.title}</label>
                <span>{props.children}</span>
            </div>
        </React.Fragment>
    )
}