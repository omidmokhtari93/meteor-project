import React from 'react'


export const SelectAll = props => {
    return <div className={`d-flex align-items-center pointer irancell-select-all ${props.className || ''}`}
        onClick={() => props.handleChange(!props.checked)}>
        <label className={`check-box pointer rounded-circle d-flex align-items-center ${props.checked ? 'bg-black' : ''}`}>
            <span className="fa fa-check font-size-10 text-white"></span>
        </label>
        <span className="font-size-13 mr-1">انتخاب همه</span>
    </div>
}