import React, { useRef, useState } from 'react'



export const RoundedInput = props => {
    const [active, setActive] = useState(false)
    const inputRef = useRef(0)
    useState(() => {
        if (props.value) {
            setActive(true)
        }
    }, [])

    const checkIsEmpty = () => {
        if (!props.value) {
            setActive(false)
        }
    }

    const activeInput = () => {
        setActive(true)
        inputRef.current.focus()
    }

    return <div
        className={`rounded-input ${active ? 'active' : ''} ${props.className || ''}`}
        onBlur={() => checkIsEmpty()}
        onClick={() => activeInput()}>
        <label className="label">{props.label}</label>
        <input type={props.type || 'text'}
            ref={inputRef}
            value={props.value}
            onChange={(e) => props.handleChange(e.target.value)}
        />
        {props.title && <label className="inner-item">{props.title}</label>}
    </div>
}