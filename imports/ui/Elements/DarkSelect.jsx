import React from 'react'



export const DarkSelect = props => {
    return <select className={`dark-select rounded-xxl dark-irancell border-0 no-ouline py-2 px-2 font-size-13 ${props.className || ''}`}
        style={{ height: props.height || '40px' }}>
        {props.options && props.options.map(x => <option key={x.value} value={x.value}>{x.text}</option>)}
    </select>
}