import React from 'react'


export const Switch = props => {
    return <div className={`form-group ${props.className || ''}`}
        style={props.style || {}}>
        <span className="switch switch-sm">
            <input type="checkbox" className="switch" id="switch-sm"
                onClick={e => props.handleChange()} />
            <label htmlFor="switch-sm">{props.label}</label>
        </span>
    </div>
}