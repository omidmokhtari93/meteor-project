import React, { useEffect, useRef, useState } from 'react'


export const Input = props => {
    let [labelStyle, setLabelStyle] = useState('')
    let [divStyle, setDivStyle] = useState('')
    let input = useRef(0)
    useEffect(() => {
        if (input.current) {
            input.current.addEventListener('focus', onFocus)
        }
        props.value && setLabelStyle('active')

        return () => input.current.removeEventListener('focus', onFocus)
    })
    const onFocus = e => {
        if (labelStyle == '') {
            setLabelStyle('active')
            setDivStyle('active')
            input.current.focus()
        }
    }
    const onblur = e => {
        setDivStyle('')
        if (props.value.trim() == '') {
            setLabelStyle('')
        }
        props.handleChange(props.name, props.value.trim())
    }

    const handleInput = (text) => {
        props.handleChange(props.name, text)
    }

    const isInvalid = props.required && props.touched && props.value.trim() == ''
    return (
        <React.Fragment>
            <div className={`irancell-input app-shadow sans ${isInvalid ? 'invalid' : ''}
            ${divStyle} ${props.className ? props.className : ''}`}
                onClick={onFocus}>
                <input type={props.type} ref={input} autoComplete="off"
                    onChange={(e) => handleInput(e.target.value)}
                    value={props.value} onBlur={() => onblur()} />
                <label className={`noselect ${labelStyle}`}>{props.label}</label>
                {isInvalid && <span className="fa fa-exclamation-circle"></span>}
            </div>
            {props.message && isInvalid &&
                <p className="irancell-input-message mb-0 mt-1 text-right ltr">{props.message}</p>}
        </React.Fragment>
    )
}