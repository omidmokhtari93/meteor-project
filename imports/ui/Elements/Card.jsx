import React from 'react'



export const Card = props => {
    return (
        <div className={`card text-right sans rounded-x app-shadow ${props.className ? props.className : ''}`}>
            {props.children}
        </div>
    )
}