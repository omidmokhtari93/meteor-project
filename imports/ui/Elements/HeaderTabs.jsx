import React from 'react'


export const HeaderTabs = props => {
    return <div className="header-tabs sans">
        <div className="card rounded-m">
            <div className="d-flex font-size-12 text-center tab">
                {props.tabs && props.tabs.map(x => (<div
                    key={x.id}
                    onClick={() => props.handleChange(x.id)}
                    className={`flex-fill w-100 ${props.selected == x.id ? 'active' : ''}`}>
                    {x.text}
                </div>))}
            </div>
            <div className="header-tabs-body p-4 text-right">
                {props.children}
            </div>
        </div>
    </div>
}