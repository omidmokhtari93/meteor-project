import React from 'react'



export const RadioButton = props => {
    //console.log(props.selected)
    const invalid = props.required && props.touched && !props.selected
    return (
        <React.Fragment>
            <div className={`irancell-radio pr-2 sans ${invalid ? 'invalid' : ''}`}>
                <label className="title">{props.label} : </label>
                <ul>
                    {props.options && props.options.map(x => {
                        return <li key={x.value}
                            className={`noselect ${props.selected == x.value ? 'active' : ''}`}
                            onClick={() => props.handleChange(props.name, x.value)}>
                            <label></label>{x.text}
                        </li>
                    })}
                </ul>
            </div>
            {props.message && invalid &&
                <p className="irancell-input-message mb-0 mt-1 text-right ltr">{props.message}</p>}
        </React.Fragment>
    )
}