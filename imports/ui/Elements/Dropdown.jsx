import React, { useEffect, useState } from 'react';


export const Dropdown = props => {
    const [active, setActive] = useState(false);
    const [options, setOptions] = useState(null)
    const [title, setTitle] = useState('')
    useEffect(() => {
        props.value && props.options.length
            ? setTitle(props.options.find(x => x.value == props.value).text)
            : setTitle(props.title)
        setOptions(<ul className="app-shadow">
            {props.options && props.options.map((x, idx) => <li key={idx}
                onClick={() => onSelect(x.value, x.text)}>{x.text}</li>)}
        </ul>)
    }, [props])

    const onSelect = (value, text) => {
        setTitle(text)
        props.handleSelect && props.handleSelect(props.name, value)
    }

    const onblur = () => {
        setActive(false)
        // props.options.length
        //     && props.handleSelect
        //     && props.handleSelect(props.name, props.value)
    }
    const invalid = !props.value && props.required && props.touched
    return (
        <React.Fragment>
            <div tabIndex="0" className={`noselect irancell-drp app-shadow sans
             ${invalid ? 'invalid' : ''}
             ${props.color ? props.color : ''}`}
                onBlur={() => onblur()}
                onClick={() => setActive(!active)}>
                <span className="fa fa-chevron-down"></span>
                <span className="drp-title noselect">{title}</span>
                {active && options}
            </div>
            {props.message && invalid &&
                <p className="irancell-input-message mb-0 mt-1 text-right ltr">{props.message}</p>}
        </React.Fragment>
    )
}