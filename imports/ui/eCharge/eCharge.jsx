import React, { useState } from 'react'
import { Card } from '../Elements/Card'
import { DarkInput } from '../Elements/DarkInput'
import { RoundedInput } from '../Elements/RoundedInput'
import { RoundedRadioButton } from '../Elements/RoundedRadioButton'
import { Switch } from '../Elements/Switch'
import { ToggleButtons } from '../Elements/ToggleButtons'
import { Container } from '../Layout/Container'
import { DarkLayout } from '../Layout/DarkLayout'
import { Button } from '../Elements/Button'
import { Banks } from './Bank'
import { Invoice } from './Invoice'
import { isMobile } from 'react-device-detect'

const charges = [
    { id: 1, text: '10,000', unit: 'ریال' },
    { id: 2, text: '20,000', unit: 'ریال' },
    { id: 3, text: '50,000', unit: 'ریال' },
    { id: 4, text: '100,000', unit: 'ریال' },
    { id: 5, text: '200,000', unit: 'ریال' },
    { id: 6, text: 'سایر مبالغ' },
]

const banks = [
    { id: 1, text: 'ملت', image: '/img/mellat.png' },
    { id: 2, text: 'توسعه تعاون', image: '/img/tosee.png' },
    { id: 3, text: 'انصار', image: '/img/ansar.png' },
    { id: 4, text: 'سینا', image: '/img/sina.png' },
    { id: 5, text: 'سینا', image: '/img/sina.png' },
]

export const ECharge = props => {
    document.title = "خرید آنلاین شارژ ایرانسل"
    const [simType, setSimType] = useState(1)
    const [phone, setPhone] = useState('')
    const [charge, setCharge] = useState(1)
    const [chargeType, setChargeType] = useState(false)
    const [chargeInput, setChargeInput] = useState('')
    const [showChargeInput, setShowChargeInput] = useState(false)
    const [email, setEmail] = useState('')
    const [selectedBank, setSelectedBank] = useState(null)

    const handleChangeCharge = (e) => {
        setCharge(e)
        e == 6 ? setShowChargeInput(true) : setShowChargeInput(false)
    }
    const bank = selectedBank ? banks.find(x => x.id == selectedBank).text : '';
    const chargeValue = charge == 6 ? chargeInput : charges.find(x => x.id == charge).text
    return (
        <DarkLayout>
            <Container>
                <div className="row position-relative">
                    <div className="col-12 col-md-10 col-lg-8 ml-auto mr-auto">
                        <Card className="p-3 p-lg-5 mt-2 mt-lg-5">
                            <div className="row text-center">
                                <div className="col-12 col-md-12 col-lg-6 px-4">
                                    <p className="font-weight-800 mb-3">خرید آنلاین شارژ ایرانسل</p>
                                    <br />
                                    <span className="font-size-12 text-muted">نوع سیم کارت</span>
                                    <ToggleButtons handleChange={setSimType}
                                        className="mt-2"
                                        selected={simType}
                                        options={[
                                            { id: 1, text: 'اعتباری' },
                                            { id: 2, text: 'دائمی' }
                                        ]}
                                    />
                                    <Switch
                                        handleChange={() => setChargeType(!chargeType)}
                                        label={chargeType ? 'شگفت انگیز' : 'معمولی'}
                                        style={{ width: '211px' }}
                                        className="text-right ml-auto mr-auto mt-4"
                                    />
                                    <RoundedInput
                                        type="tel"
                                        value={phone}
                                        handleChange={setPhone}
                                        label="شماره تلفن همراه"
                                    />
                                    <p className="text-right font-size-12 text-muted mt-4 pr-1">مبلغ شارژ :</p>
                                    <RoundedRadioButton
                                        options={charges}
                                        selected={charge}
                                        handleChange={handleChangeCharge}
                                    />
                                    {showChargeInput && <DarkInput
                                        value={chargeInput}
                                        type="tel"
                                        handleChange={setChargeInput}
                                        label="ریال"
                                        placeholder="مبلغ شارژ به ریال"
                                    />}
                                    <DarkInput
                                        className="mt-4"
                                        value={email}
                                        handleChange={setEmail}
                                        placeholder="آدرس ایمیل"
                                    />
                                    <Banks
                                        options={banks}
                                        className="mt-3"
                                        selected={selectedBank}
                                        handleChange={setSelectedBank}
                                    />
                                    <Button className="mt-3">
                                        پرداخت و شارژ
                                    </Button>
                                </div>
                                <div className="col-12 col-md-12 col-lg-6 px-4">
                                    {!isMobile && <Invoice
                                        className="mt-4 mt-lg-0"
                                        simType={simType}
                                        phone={phone}
                                        chargeType={chargeType}
                                        charge={chargeValue}
                                        email={email}
                                        bank={bank}
                                    />}
                                </div>
                            </div>
                        </Card>
                    </div>
                </div>
            </Container>
        </DarkLayout>
    )
}