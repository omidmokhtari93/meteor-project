import React from 'react'



export const Banks = props => {
    return <div className={props.className || ''}>
        {props.options && props.options.map(x => {
            return <div
                title={x.text}
                onClick={() => props.handleChange(x.id)}
                className={`banks ${props.selected == x.id ? 'active' : ''}`}
                key={x.id}>
                <img src={x.image} />
            </div>
        })}
    </div>
}