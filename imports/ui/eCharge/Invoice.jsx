import React from 'react'
import { numberWithCommas } from '../../services/comma-seprator'


export const Invoice = props => {
    return <div className={`charge-invoice ${props.className || ''}`}>
        <div className="title">
            فاکتور نهایی
        </div>
        <div className="body">
            <p className="text-right font-size-11 text-muted mb-2">نوع سیم کارت : </p>
            <p className="font-weight-800 text-right font-size-13">{props.simType == 2 ? 'دائمی' : 'اعتباری'}</p>
            <p className="text-right font-size-11 text-muted mb-2">مستقیم به شماره : </p>
            <p className="font-weight-800 text-right font-size-13">{props.phone}</p>
            <p className="text-right font-size-11 text-muted mb-2">مبلغ شارژ (با احتساب مالیات بر ارزش افزوده) : </p>
            <p className="font-weight-800 text-right font-size-13">{numberWithCommas(props.charge)} {props.charge && 'ریال'}</p>
            <p className="text-right font-size-11 text-muted mb-2">نوع شارژ : </p>
            <p className="font-weight-800 text-right font-size-13">{props.chargeType ? 'شگفت انگیز' : 'معمولی'}</p>
            <p className="text-right font-size-11 text-muted mb-2">ایمیل : </p>
            <p className="font-weight-800 text-right font-size-13">{props.email}</p>
            <p className="text-right font-size-11 text-muted mb-2">نام بانک : </p>
            <p className="font-weight-800 text-right font-size-13">{props.bank}</p>
        </div>
    </div>
}