import React from 'react';



export const ProductFeatures = props => {
    //console.log(props)
    return (
        <div className="product-features">
            <p className="mb-2"><strong>ویژگی های محصول</strong></p>
            <p className="text-muted mb-2" style={{ fontSize: '12px' }}>توضیحات مربوط به ویژگی های محصول در این قسمت قرار میگیرد</p>
            <table>
                <tbody>
                    {props.features.map(x => {
                        return <tr key={x.title}>
                            <td>{x.title}</td>
                            <td>{x.description}</td>
                        </tr>
                    })}
                </tbody>
            </table>
        </div>
    )
}