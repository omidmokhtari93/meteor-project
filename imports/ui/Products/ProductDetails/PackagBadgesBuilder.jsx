import React from 'react'
import { PackageBadge } from './PackageBadge'



export const PackageBadegsBuilder = props => {
    return <div className="row mb-0">
        {props.packages.map((x, idx) => {
            return <div className="col-12 col-lg-6 mb-3" key={idx}>
                <PackageBadge
                    selectedPackage={props.selectedPackage}
                    handleSelectPackage={props.handleSelectPackage}
                    {...x}
                />
            </div>
        })}
    </div>
}