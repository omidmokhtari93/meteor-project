import React, { useState } from 'react'



export const ProductPhotos = props => {
    const [imgIndex, setImgIndex] = useState(0)
    //console.log(imgIndex)
    return (
        <React.Fragment>
            <div>
                <img src={props.images[imgIndex]} alt="" className="img-fluid" />
            </div>
            <div className="mb-0 product-photos">
                {props.images.map((x, idx) => {
                    return <div key={idx} className={`images ${imgIndex == idx ? 'selected' : ''}`}
                        onClick={() => setImgIndex(idx)}
                        style={{ backgroundImage: `url(${x})` }}>
                    </div>
                })}
            </div>
        </React.Fragment>
    )
}