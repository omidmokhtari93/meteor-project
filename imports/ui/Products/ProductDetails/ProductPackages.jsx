import React, { useState } from 'react'
import { PackageBadegsBuilder } from './PackagBadgesBuilder'
import { Tabs } from '../../Elements/Tabs'

const tabs = [
    { text: 'سیم کارت اعتباری', command: 'postpaid' },
    { text: 'سیم کارت دائمی', command: 'prepaid' },
]

export const ProductPackages = props => {
    const [simType, setSimType] = useState('postpaid')
    const selectedCategory = props.packages.filter(x => x.type == simType)
    return (
        <div className="product-packages">
            <p className="mb-2"><strong>انتخاب بسته ترکیبی</strong></p>
            <p className="text-muted">جهت خرید مودم اینترنت همراه لازم است یک بسته ترکیبی از بین بسته های زیر انتخاب کنید</p>
            <Tabs tabs={tabs} toggleTabs={setSimType} selected={simType} className="border" />
            <div className="simcards">
                <div className="sim-price mb-3">
                    <i className="fa fa-tags ml-3"></i>
                    <span className="text">قیمت سیم کارت {simType == 'prepaid' ? 'دائمی' : 'اعتباری'}</span>
                    <span className="price">{simType == 'prepaid' ? '60,000' : '20,000'} تومان</span>
                </div>
                <PackageBadegsBuilder packages={selectedCategory}
                    selectedPackage={props.selectedPackage}
                    handleSelectPackage={props.handleSelectPackage}
                />
            </div>
        </div>
    )
}