import React, { useState } from 'react'
import { cart } from '../../../services/cart.service'
import { notify } from '../../../services/notify.service'
import { CheckBox } from '../../Elements/CheckBox'
import { Card } from '../../Elements/Card'
import { useTracker } from 'meteor/react-meteor-data'


export const AddToCart = props => {
    const [coverage, setCoverage] = useState(false)
    const [rules, setRules] = useState(false)
    const user = useTracker(() => Meteor.user())
    const selectedPackage = props.product.packages.find(x => x._id == props.packageId)
    const selectedProdcut = {
        productId: props.product._id,
        title: props.product.title,
        price: props.product.price,
        image: props.product.images[0],
        package: selectedPackage,
        oldPrice: props.product.oldPrice,
        quantity: 1
    };
    const handleAddToCart = e => {
        if (!props.packageId) {
            notify('حداقل یک بسته را انتخاب نمایید', 'danger')
            return
        }
        if (user) {
            Meteor.call('cart.add', selectedProdcut, (err, res) => {
                if (err) {
                    notify(err.message, 'danger')
                }
                if (res) {
                    notify(res.message, res.type)
                }
            })
            return
        } else {
            cart.add(selectedProdcut)
            notify('به سبد خرید اضافه شد', 'success')
        }
    }
    //console.log(cart.get())
    return (
        <Card className="mt-3 add-to-cart noselect">
            <CheckBox className="mb-2" checked={coverage} handleChange={() => setCoverage(!coverage)}
                title="تایید میکنم که در محدوده پوشش دهی هستم" id="coverage" >
                <span className="text-danger mr-1 bold">*</span>
            </CheckBox>
            <CheckBox className="mb-3" checked={rules} handleChange={() => setRules(!rules)}
                title="قوانین و مقررات را قبول کرده و قبول دارم" id="rules" >
                <span className="text-danger mr-1 bold">*</span>
                <a href="#">مشاهده قوانین و مقررات</a>
            </CheckBox>
            <p className="text-muted mb-3">
                <span className="fa fa-warning ml-1"></span>
                    با هر کد ملی امکان خرید حداکثر دو مودم اینترنت نسل 4 ثابت به همراه سیم کارت اعتباری در ماه امکان پذیر می باشد
            </p>
            <div className="mb-3">
                <button disabled={rules == true && coverage == true ? false : true}
                    onClick={handleAddToCart}>
                    <label><i className="fa fa-shopping-cart"></i></label>
                    افزودن به سبد خرید
                </button>
            </div>
            <div className="call">
                بعد از خرید مودم های ایرانسل, جهت فعال سازی یا هر گونه سوال با مرکز تماس ایرانسل به شماره 09377070000 تماس بگیرید و از مراجعه حضوری به نمایندگی های ایرانسل خودداری کنید.
                در صورت مراجعه به نمایندگی ها و فعال سازی مودم به صورت حضوری, به دلایل مسائل فنی امکان فعال سازی بسته اولیه وجود نخواهد داشت.
            </div>
        </Card>
    )
}