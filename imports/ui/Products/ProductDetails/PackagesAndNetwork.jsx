import React, { useState } from 'react'
import { Card } from '../../Elements/Card'
import { ProductPackages } from './ProductPackages'
import { Tabs } from '../../Elements/Tabs'

const tabs = [
    { text: 'انتخاب بسته', command: 'packages' },
    { text: 'پوشش شبکه', command: 'network' }
]

export const PackagesAndNetwork = props => {
    const [selectedTab, setSelected] = useState('packages')
    return (
        <Card className="mt-3">
            <Tabs tabs={tabs} toggleTabs={setSelected} selected={selectedTab} />
            {selectedTab == 'packages' && <ProductPackages
                packages={props.packages}
                selectedPackage={props.selectedPackage}
                handleSelectPackage={props.handleSelectPackage}
            />}
            {/* <ProductNetwork /> */}
        </Card>
    )
}