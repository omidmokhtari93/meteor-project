import React from 'react'
import { numberWithCommas } from '../../../services/comma-seprator'

export const ProductInformation = props => {
    return (
        <React.Fragment>
            <h5 className="mb-3"><strong>{props.title}</strong></h5>
            {props.description.map(desc => <p className="mb-1" key={desc}>{desc}</p>)}
            <p className="text-success mt-3">ارسال رایگان به سراسر ایران</p>
            <div className="text-left">
                <div className="d-inline-block price-badge">
                    <span className="text-muted">قیمت پایه مودم  : </span>
                    <strong>{numberWithCommas(props.price)} تومان</strong>
                </div>
            </div>
            <hr className="mt-4" />
            <div className="list-inline ltr text-right product-footer">
                <div className="list-inline-item mr-3 mb-1">
                    <span className="footer-text">پرداخت با همه کارت های شبکه شتاب</span>
                    <i className="footer-badge fa fa-credit-card"></i>
                </div>
                <div className="list-inline-item mr-3 mb-1">
                    <span className="footer-text">ارسال در سریع ترین زمان ممکن</span>
                    <i className="footer-badge fa fa-clock-o"></i>
                </div>
                <div className="list-inline-item mr-3 mb-1">
                    <span className="footer-text">ارسال به همه نقاط ایران</span>
                    <i className="footer-badge fa fa-truck"></i>
                </div>
            </div>
        </React.Fragment>
    )
}