import React, { useState } from 'react'
import { Card } from '../../Elements/Card'
import { ProductFeatures } from './ProductFeatures'
import { ProductSetting } from './ProductSetting'
import { Tabs } from '../../Elements/Tabs'

const tabs = [
    { text: 'ویژگی های مودم', command: 'modem-features' },
    { text: 'تنظیمات مودم', command: 'modem-setting' }
]

export const FeaturesAndSetting = props => {
    const [selectedTab, setSelected] = useState('modem-features')
    return (
        <Card className="mt-3">
            <Tabs tabs={tabs} toogleTabs={setSelected} selected={selectedTab} />
            {selectedTab == 'modem-features'
                ? <ProductFeatures features={props.features} />
                : <ProductSetting />}
        </Card>
    )
}