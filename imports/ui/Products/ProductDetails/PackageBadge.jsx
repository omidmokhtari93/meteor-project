import React from 'react'
import { numberWithCommas } from '../../../services/comma-seprator'



export const PackageBadge = props => {
    //console.log(props)
    return (
        <div className={`package pointer app-shadow ${props._id == props.selectedPackage ? 'selected' : ''}`}
            onClick={() => props.handleSelectPackage(props._id)}>
            <label className={`select`}></label>
            <span className={`title ${props.oldPrice && 'discount-color'}`}>{props.name}</span>
            {props.oldPrice && <span className="fa fa-certificate discount"></span>}
            {props.oldPrice && <span className="old-price">{numberWithCommas(props.oldPrice)} تومان</span>}
            <span className="price">{numberWithCommas(props.price)} تومان</span>
            <label className="package-size ltr">
                {props.amount}<span>GB</span>
            </label>
        </div>
    )
}