import React, { useEffect, useState } from 'react'
import { ProductPhotos } from './ProductDetails/ProductPhotos'
import { withRouter } from 'react-router-dom';
import { ProductInformation } from './ProductDetails/ProductInformation';
import { Card } from '../Elements/Card';
import { PackagesAndNetwork } from './ProductDetails/PackagesAndNetwork';
import { FeaturesAndSetting } from './ProductDetails/FeaturesAndSetting';
import { AddToCart } from './ProductDetails/AddToCart';
import { DarkLayout } from '../Layout/DarkLayout';
import { Container } from '../Layout/Container';

export const ProductDetailes = withRouter(props => {
    const [selectedPackage, setSelectedPackage] = useState('')
    const [product, setProduct] = useState(null)

    useEffect(() => {
        Meteor.call('product.GetDetailes', props.match.params.id, (err, res) => {
            if (res) {
                setProduct(res)
            }
        });
    }, [props.match.params.id])
    return product && <DarkLayout>
        <Container>
            <Card>
                <div className="row m-0">
                    <div className="col-md-4 product-photos-reponsive">
                        <ProductPhotos images={product.images} />
                    </div>
                    <div className="col-md-8 device-detailes">
                        <ProductInformation
                            title={product.title}
                            description={product.description}
                            price={product.price}
                        />
                    </div>
                </div>
            </Card>
            <PackagesAndNetwork
                packages={product.packages}
                network={props.network}
                selectedPackage={selectedPackage}
                handleSelectPackage={setSelectedPackage}
            />
            <FeaturesAndSetting features={product.features} />
            <AddToCart product={product} packageId={selectedPackage} />
        </Container>
    </DarkLayout>
})