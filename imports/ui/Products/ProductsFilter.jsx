import React, { useEffect, useState } from 'react';

const modemCategory = [
    { title: 'مودم اینترنت نسل 4 ثابت (TD-LTE)', command: 'fixed_modem' },
    { title: 'مودم همراه 3G / 4G / 4.5G', command: 'portable_modem' }
]
const simCategory = [
    { title: 'سیم کارت دائمی', command: 'sim_prepaid' },
    { title: 'سیم کارت اعتباری', command: 'sim_postpaid' }
]

export const ProductsFilter = props => {
    const [filter, setFilter] = useState('')
    useEffect(() => {
        props.selected == 'fixed_modem' ? setFilter(modemCategory)
            : props.selected == "sim_prepaid" ? (setFilter(simCategory)) : null;
    }, [props.selected])

    //console.log(props)
    return (
        <div className="row sans">
            <div className="col-12 col-sm-12 col-lg-8 ml-auto mr-auto">
                <div className="card mb-4 product-filter app-shadow">
                    <ul className="list-inline mb-0 text-center pr-0">
                        {filter && filter.map((x) => {
                            return <li key={x.command}
                                className={`mr-0 list-inline-item text-center ${props.selected == x.command && 'list-active'}`}
                                onClick={() => props.toggleMenu(x.command)}>
                                {x.title}
                            </li>
                        })}
                    </ul>
                </div>
            </div>
        </div>
    )
}