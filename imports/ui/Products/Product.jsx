import React from 'react'
import { withRouter } from 'react-router-dom'
import { numberWithCommas } from '../../services/comma-seprator'



export const Product = withRouter(props => {
    const discont = props.oldPrice ? Math.round((props.price - props.oldPrice) / props.price * 100) : null
    return (
        <div className="card rounded-xl p-4 app-shadow sans text-right product position-relative noselect">
            {discont && <label className="discount-badge noselect">{discont} % تخفیف</label>}
            <img src={props.images[0]} alt={props.title} className="img-fluid rounded-xl" />
            <div className="pt-3">
                <h5 className="title">{props.title}</h5>
                {props.description.map(desc => <p key={desc}>{desc}</p>)}
                {props.available ? <h4 className="price">
                    {props.oldPrice && <span className="old-price">{numberWithCommas(props.oldPrice)} تومان</span>}
                    <span className="main-price">{numberWithCommas(props.price)} تومان</span>
                </h4> : <h4 className="unavailable">ناموجود !</h4>}
                {props.available ? <button onClick={() => props.history.push('/product/' + props._id)}>مشاهده جزئیات</button>
                    : <button className="unavailable">موجود شد به من اطلاع بده !</button>}
            </div>
        </div>
    )
})