import React, { useEffect, useState } from 'react'
import { ProductsFilter } from './ProductsFilter';
import { Product } from './Product';
import { DarkLayout } from '../Layout/DarkLayout';
import { Container } from '../Layout/Container'
import { withRouter } from 'react-router-dom';

const Products = props => {
    const path = props.location.pathname
    path.includes('devices')
        ? document.title = "خرید مودم"
        : path.includes('simcard')
            ? document.title = 'خرید سیم کارت' : '';
    const [productCategory, setProductCategory] = useState('')
    const [products, setProducts] = useState(null)
    useEffect(() => {
        const produtCat = path.includes('devices') ? 'modem' : path.includes('simcard') ? 'simcard' : '';
        Meteor.call('products.getByCategory', produtCat, (err, res) => {
            setProducts(res)
            setProductCategory(produtCat == 'modem' ? 'fixed_modem' : produtCat == 'simcard' ? 'sim_prepaid' : '')
        })
    }, [path])

    const filteredProducts = products && products.filter(x => x.category == productCategory)

    //console.log(products)
    return products && <DarkLayout>
        <Container>
            <ProductsFilter selected={productCategory} toggleMenu={setProductCategory} />
            <div className="row">
                {filteredProducts.map(x => {
                    return <div key={x._id} className="col-12 col-md-6 col-lg-4 product-margin">
                        <Product {...x} />
                    </div>
                })}
            </div>
        </Container>
    </DarkLayout>

}

export default withRouter(Products);