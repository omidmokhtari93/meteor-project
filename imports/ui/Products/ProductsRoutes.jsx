import React from 'react'
import { Route, Switch } from 'react-router-dom'
import Products from './Products'
import { ProductDetailes } from './ProductDetailes'

export const ProductsRoutes = props => {
    return (
        <Switch>
            <Route path="/products/devices" exact render={() => <Products />} />
            <Route path="/products/simcards" render={() => <Products />} />
            <Route path="/product/:id" render={() => <ProductDetailes />} />
        </Switch>
    )
}