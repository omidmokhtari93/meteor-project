import React from 'react'
import { NavLink } from 'react-router-dom'
import { NavItem } from './NavItem'


export const NavbarItems = props => {
    return (
        <React.Fragment>
            <NavItem>
                <NavLink className="nav-link" to="/" exact activeClassName="active">
                    صفحه اصلی
                </NavLink>
            </NavItem>
            <NavItem>
                <NavLink className="nav-link" to="/products/devices" activeClassName="active">
                    دستگاه ها
                </NavLink>
            </NavItem>
            <NavItem>
                <NavLink className="nav-link" to="/products/simcards" activeClassName="active">
                    سیمکارت
                </NavLink>
            </NavItem>
            <NavItem>
                <NavLink className="nav-link" to="/services" activeClassName="active">
                    خدمات
                </NavLink>
            </NavItem>
            <NavItem>
                <NavLink className="nav-link" to="/packages/gsm" activeClassName="active">
                    بسته ها
                </NavLink>
            </NavItem>
            <NavItem>
                <a className="nav-link" href="#">حراج شماره های رند</a>
            </NavItem>
            <NavItem>
                <a className="nav-link" href="#">تخفیفات و پیشنهادات ویژه</a>
            </NavItem>
        </React.Fragment>
    )
}