import React from 'react'
import { Backdrop } from '../Elements/Backdrop'
import { NavbarItems } from './NavBarItems'
import NavOtherItems from './NavOtherItems'



export const SideNavbar = props => {
    return (
        <React.Fragment>
            <div className="side-navbar app-shadow" style={{
                left: props.show ? '0' : '-300px'
            }}>
                {props.show && <span className="close"
                    onClick={() => props.closeSideMenu()}>✕</span>}
                <ul className="navbar-nav other" onClick={() => props.closeSideMenu()}>
                    <NavOtherItems />
                </ul>
                <hr />
                <ul className="navbar-nav pr-0 text-right" onClick={() => props.closeSideMenu()}>
                    <NavbarItems />
                </ul>
                <img src="/img/irancell_new.png" alt="" width="50" />
            </div>
            {props.show && <Backdrop onClick={() => props.closeSideMenu()} />}
        </React.Fragment>
    )
}