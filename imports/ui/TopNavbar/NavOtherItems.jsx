import React from 'react'
import { connect } from 'react-redux'
import { NavLink, withRouter } from 'react-router-dom'
import { useTracker } from 'meteor/react-meteor-data';
import { UserCartCollection } from '../../methods/links';
import { NavItem } from './NavItem';


const NavOtherItems = props => {
    const logoutUser = () => {
        Meteor.logout();
        props.history.push('/')
    }

    const { cartCount, user } = useTracker(() => {
        if (Meteor.user()) {
            const handler = Meteor.subscribe('cart.get');
            if (!handler.ready()) {
                return { cartCount: 0, user: {} }
            }
            const cart = UserCartCollection.findOne({})
            return {
                user: Meteor.user(),
                cartCount: cart && cart.products.length
            }
        } else {
            return { cartCount: props.cartCount }
        }
    })
    return (
        <React.Fragment>
            <NavItem>
                <NavLink to="/cart" className="nav-link text-dark position-relative" activeClassName="active">
                    <span className="fa fa-shopping-cart">
                        {cartCount > 0 && <span className="cart-counter sans text-center">{cartCount}</span>}
                    </span>
                </NavLink>
            </NavItem>
            <NavItem>
                <a className="nav-link text-dark" href="#">
                    <i className="fa fa-users"></i> مشترکان تجاری
                </a>
            </NavItem>
            <NavItem>
                <a className="nav-link text-dark" href="#">
                    {user
                        ? <React.Fragment>
                            <span className="mt-1 fa fa-power-off ml-2" onClick={() => logoutUser()}></span>
                            <span className="align-middle">{user.username}</span></React.Fragment>
                        : <React.Fragment>
                            <span onClick={() => props.setShowLogin(!props.showLogin)} className="fa fa-user"></span>
                        </React.Fragment>}
                </a>
            </NavItem>
        </React.Fragment>
    )
}

const mapStateToProps = state => ({ cartCount: state.cart })

export default connect(mapStateToProps)(withRouter(NavOtherItems))