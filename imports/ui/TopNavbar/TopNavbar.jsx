import React, { useState } from 'react'
import { Login } from '../PurchaseComponents/Cart/Login'
import { NavbarItems } from './NavBarItems'
import NavOtherItems from './NavOtherItems'
import { SideNavbar } from './SideNavbar'

export const TopNavbar = props => {
    const [showSide, setShowSide] = useState(false)
    const [showLogin, setShowLogin] = useState(false)
    return (
        <React.Fragment>
            <Login show={showLogin} closeModal={setShowLogin} />
            <nav className="noselect sticky-top navbar navbar-expand-xl navbar-dark fixed-top topnav pt-2 pb-2 pr-1 sans">
                <a className="navbar-brand" href="#">
                    <img src="/img/irancell_new.png" alt="" width="50" />
                </a>
                <button className="navbar-toggler" onClick={() => setShowSide(!showSide)}>
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse top-font">
                    <ul className="navbar-nav ml-auto pr-3">
                        <NavbarItems />
                    </ul>
                    <ul className="navbar-nav ml-auto pr-3">
                        <NavOtherItems showLogin={showLogin} setShowLogin={setShowLogin}/>
                    </ul>
                </div>
                <SideNavbar
                    show={showSide}
                    closeSideMenu={() => setShowSide(!showSide)}
                />
            </nav>
        </React.Fragment>
    )
}