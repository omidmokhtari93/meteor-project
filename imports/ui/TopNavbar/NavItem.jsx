import React from 'react'



export const NavItem = props => {
    return <li className="nav-item">{props.children}</li>
}