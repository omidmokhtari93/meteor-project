import React, { useEffect, useState } from 'react'
import { withRouter } from 'react-router-dom'
import { Collapse } from '../Elements/Collapse'
import { Container } from '../Layout/Container'
import { DarkLayout } from '../Layout/DarkLayout'
import { PackageBox } from './PackageBox'
import { SearchPackages } from './SearchPackages'
import { SpecialOffer } from './SpecialOffer'

export const Packages = withRouter(props => {
    const [collapsedTerms, setCollapserTerms] = useState(false)
    const [collapsedMostSales, setCollapseMostSales] = useState(false)
    const [collapsedSpecialOffer, setSpecialOffer] = useState(false)
    const [packages, setPackages] = useState([])

    const setFilters = (filters) => {
        //console.log(filters)
        Meteor.call('packages.get', {
            category: props.match.params.type,
            ...filters
        }, (err, res) => {
            if (res) {
                setPackages(res)
            }
        })
    }

    return <DarkLayout>
        <Container>
            <div className="mt-lg-5 mt-0 sans">
                <Collapse
                    collapsed={collapsedTerms}
                    handleChange={setCollapserTerms}
                    title="خرید آنلاین بسته های اینترنت همراه"
                    description=" قبل از خرید بسته اینترنت همراه, توضیه میکنیم به این نکات توجه کنیم.
                    قبل از خرید بسته اینترنت همراه, توصیه می کنیم به این نکات توجه کنید.">
                    <p className="mt-4 text-right">تست</p>
                </Collapse>
                <Collapse
                    className="mt-4"
                    collapsed={collapsedMostSales}
                    handleChange={setCollapseMostSales}
                    title="پر فروش ترین بسته ها">
                    <div className="row mt-4 m-0">
                        {[...Array(4)].map((x, idx) => {
                            return <div key={idx} className="col-12 col-md-6 col-lg-3 py-2">
                                <PackageBox
                                    id="5"
                                    handleClick={(id) => props.history.push('/packages/purchase' + id)}
                                    title="11 گیگابایت 30 روزه"
                                    trafficLimit={2000}
                                    validity="Hourly"
                                    codeTitle="کد دستوری"
                                    ussdCode="*555*5*6*4#"
                                    price="55000"
                                    buttonText="خرید آنلاین"
                                />
                            </div>
                        })}
                    </div>
                </Collapse>
                <Collapse
                    collapsed={collapsedSpecialOffer}
                    handleChange={setSpecialOffer}
                    title="پیشنهادهای ویژه"
                    className="mt-4">
                    <div className="row mt-4 m-0 irancell-special-offer rounded-m">
                        <div className="col-12 col-md-12 col-lg-7 ml-auto mr-auto py-2">
                            <SpecialOffer />
                        </div>
                    </div>
                </Collapse>
                <div className="row">
                    <div className="col-12">
                        <p className="text-right py-lg-4 font-weight-800 pr-3 mb-0 py-3">جستجوی بسته ها</p>
                    </div>
                    <div className="col-12 col-md-12 col-lg-3 py-1">
                        <SearchPackages
                            handleChange={setFilters}
                        />
                    </div>
                    <div className="col-12 col-md-12 col-lg-9">
                        <div className="row m-0">
                            {packages.map((x, idx) => {
                                return <div key={idx} className="col-12 col-md-6 col-lg-4 py-1">
                                    <PackageBox
                                        {...x}
                                        handleClick={(id) => props.history.push('/packages/purchase/' + id)}
                                        className="mb-4"
                                        codeTitle="کد دستوری"
                                        buttonText="خرید آنلاین"
                                    />
                                </div>
                            })}
                        </div>
                    </div>
                </div>
            </div>
        </Container>
    </DarkLayout>
})