import React from 'react'
import { Button } from '../Elements/Button'
import { DarkInput } from '../Elements/DarkInput'


export const SpecialOffer = props => {
    return <div className="card p-3 text-right rounded-m app-shadow">
        <div className="row m-0">
            <div className="col-12 col-md-12 col-lg-2 text-center d-flex align-items-center p-0 mb-3 mb-lg-0">
                <img src="/img/special-offer.png" width="60" className="m-auto" />
            </div>
            <div className="col-12 col-md-12 col-lg-5 d-flex align-items-center mb-3 mb-lg-0">
                <div className="m-auto m-lg-0">
                    <p className="font-size-14 font-weight-800 color-dark mb-1">برای مشاهده پیشنهادهای ویژه</p>
                    <p className="mb-0 font-size-16 font-weight-800">شماره موبایل خود را وارد کنید</p>
                </div>
            </div>
            <div className="col-12 col-md-12 col-lg-5">
                <DarkInput
                    height="35px"
                    placeholder="شماره موبایل خود را وارد کنید"
                />
                <Button height="35px" className="p-1">دریافت کد تایید</Button>
            </div>
        </div>
    </div>
}