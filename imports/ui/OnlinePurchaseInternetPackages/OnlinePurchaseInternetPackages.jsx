import React, { useEffect, useState } from 'react'
import { DarkLayout } from '../Layout/DarkLayout';
import { Container } from '../Layout/Container'
import { SimpleInput } from '../Elements/SimpleInput';
import { Switch2 } from '../Elements/Switch2';
import { Banks } from './Banks';
import { Button } from '../Elements/Button';
import { withRouter } from 'react-router-dom';
import { validities } from './models';
import { numberWithCommas } from '../../services/comma-seprator';

const banks = [
    { id: 1, text: 'ملت', image: '/img/mellat.png' },
    { id: 2, text: 'توسعه تعاون', image: '/img/tosee.png' },
    { id: 3, text: 'انصار', image: '/img/ansar.png' },
    { id: 4, text: 'سینا', image: '/img/sina.png' }
]

const volume = e => {
    if (e < 1000) {
        return `${e} مگابایت`
    }
    return `${e / 1000} گیگابایت`
}
export const OnlinePurchaseInternetPackages = withRouter(props => {
    const [coupons, setCoupens] = useState(false)
    const [bank, setBank] = useState(1)
    const [package, setPackage] = useState(null)
    useEffect(() => {
        Meteor.call('packages.getOne', props.match.params.id, (err, res) => {
            //console.log(res)
            setPackage(res)
        })
    }, [])
    const time = package && validities.find(x => x.id == package.validity).text
    return <DarkLayout>
        <Container>
            {package && <div className="row mt-lg-5 mt-0 sans">
                <div className="col-12 col-md-10 col-lg-9 ml-auto mr-auto">
                    <div className="card rounded-m pt-3 pb-5 px-4 px-lg-5">
                        <p className="text-center font-size-13 font-weight-800 mt-3">
                            خرید آنلاین بسته اینترنت همراه, ویژه مشترکین ایرانسل
                        </p>
                        <small className="text-muted font-size-11 text-right px-lg-4 px-2">
                            شما می توانید بسته اینترنت مورد نظر خود را برای هر شماره ای که مایل باشید بخرید. لطفا توجه داشته باشید
                            که مبلغ مالیات اضافه شده به بسته , معادل مالیاتی است که در هنگام خرید شارژ کی پردازید. بنابراین از هر روشی
                            که برای خرید بسته اقدام کنید, هزینه یکسانی خواهید پرداخت.
                        </small>
                        <div className="row mt-lg-5 mt-3 text-right">
                            <div className="col-12 col-md-12 col-lg-6 mb-4 mb-lg-0">
                                <div className="charge-box rounded-m">
                                    <h5 className="text-center font-weight-800 mt-4">
                                        {volume(package.trafficLimit) + ' ' + time}
                                    </h5>
                                    <p className="text-center font-size-14 font-weight-800 mt-4 mb-1">{package.title}</p>
                                    <div className="dotted" />
                                    <div className="px-3 font-size-13 font-weight-800">
                                        <div className="d-flex">
                                            <p className="ml-auto">پوشش شبکه : </p>
                                            <p className="mr-auto">{package.networkCoverage}</p>
                                        </div>
                                        <div className="d-flex">
                                            <p className="ml-auto">کد دستوری : </p>
                                            <p className="mr-auto ltr">{package.ussdCode}</p>
                                        </div>
                                        <div className="d-flex">
                                            <p className="ml-auto">قیمت بسته : </p>
                                            <p className="mr-auto irancell-blue-text">
                                                {numberWithCommas(package.price)}
                                                تومان
                                            </p>
                                        </div>
                                        <div className="d-flex">
                                            <p className="ml-auto">مالیات بر ارزش افزوده : </p>
                                            <p className="mr-auto">900 تومان</p>
                                        </div>
                                    </div>
                                    <div className="footer rounded-m ml-1 mr-1 mb-1 p-3 d-flex font-size-16 font-weight-800">
                                        <p className="ml-auto mb-0">جمع مبلغ پرداختی : </p>
                                        <p className="mr-auto mb-0">{numberWithCommas(package.price + 900)} تومان</p>
                                    </div>
                                </div>
                            </div>
                            <div className="col-12 col-md-12 col-lg-6">
                                <p className="font-size-14 text-right font-weight-800 mb-2">شماره موبایل خود را وارد کنید</p>
                                <SimpleInput
                                    height="50px"
                                    placeholder="شماره موبایل"
                                />
                                <div className="d-flex align-items-center mt-4">
                                    <p className="ml-2 mb-2">استفاده از رمز کوپن</p>
                                    <Switch2 handleChange={() => setCoupens(!coupons)} />
                                </div>
                                {coupons ? <React.Fragment>
                                    <SimpleInput height="50px" placeholder="رمز کوپن خود را وارد نمایید" />
                                    <p className="irancell-blue-text font-size-10 mt-1">
                                        جهت استفاده از کوپن, قیمت بسته انتخابی باید
                                        کوچکتر/ مساوی موجودی کوپن باشد
                                        و در صورت خرید سایر محصولات محدودیتی وجود ندارد.
                                    </p>
                                </React.Fragment>
                                    : <React.Fragment>
                                        <br />
                                        <br />
                                        <br />
                                    </React.Fragment>}
                                <Banks options={banks} selected={bank} handleChange={setBank} />
                                <Button className="mt-4" height="45px">
                                    تایید و پرداخت
                                </Button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>}
        </Container>
    </DarkLayout>
})