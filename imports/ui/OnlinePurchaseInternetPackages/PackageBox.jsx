import React from 'react'
import { numberWithCommas } from '../../services/comma-seprator'
import { Button } from '../Elements/Button'
import { validities } from './models'
const volume = e => {
    if (e < 1000) {
        return `${e} مگابایت`
    }
    return `${e / 1000} گیگابایت`
}
export const PackageBox = props => {
    //console.log(props)
    const time = props.validity && validities.find(x => x.id == props.validity).text
    return <div className={`card irancell-package-box rounded-m text-center app-shadow bg-white ${props.className || ''}`}>
        <p className="font-weight-800 mt-4 mt-lg-3 font-size-14">{volume(props.trafficLimit) + ' ' + time}</p>
        <p className="font-size-11 font-weight-800 mt-4 mt-lg-3">{props.title}</p>
        <div className="d-flex font-size-10 px-4 py-1 color-dark">
            <label className="ml-auto mb-0">{props.codeTitle}</label>
            <label className="mr-auto mb-0 ltr">{props.ussdCode}</label>
        </div>
        <p className="py-2 mb-0">
            <span className="font-weight-800 font-size-15">{numberWithCommas(props.price)}</span>
            <span className="font-size-13 mr-1">تومان</span>
        </p>
        <div className="row m-0">
            <div className="col-7 ml-auto mr-auto pt-2 pb-4">
                <Button height="26px"
                    onClick={() => props.handleClick(props._id)}
                    className="p-1 d-inline-block m-auto">
                    {props.buttonText}
                </Button>
            </div>
        </div>
    </div>
}