export const charges = [
    { id: 'PREPAID', text: 'اعتباری' },
    { id: 'POSTPAID', text: 'دائمی' }
]
export const validities = [
    { id: 'Hourly', text: 'ساعتی' },
    { id: 'Daily', text: 'روزانه' },
    { id: '3-Day', text: '3 روزه' },
    { id: 'Weekly', text: 'هفتگی' },
    { id: '15-Day', text: '15 روزه' },
    { id: 'Monthly', text: '30 روزه' },
    { id: '2-Months', text: '60 روزه' },
    { id: '3-Months', text: '90 روزه' },
    { id: '4-Months', text: '120 روزه' },
    { id: '6-Months', text: '180 روزه' },
    { id: 'Yearly', text: 'یکساله' },
    { id: 'NoLimit', text: 'نامحدود' },
]
export const traffics = [
    { id: 500, text: 'تا 500 مگابایت' },
    { id: 1000, text: 'تا 1 گیگابایت' },
    { id: 2000, text: 'تا 2 گیگابایت' },
    { id: 5000, text: 'تا 5 گیگابایت' },
    { id: 10000, text: 'تا 10 گیگابایت' },
    { id: 100000, text: 'بیشتر از 10 گیگابایت' },
]