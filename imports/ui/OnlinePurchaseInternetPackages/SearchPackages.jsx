import React, { useEffect, useState } from 'react'
import { RoundedRadioButton } from '../Elements/RoundedRadioButton'
import { SelectAll } from '../Elements/SelectAll'
import { traffics, validities, charges } from './models'

export const SearchPackages = props => {
    const [simType, setSimType] = useState('PREPAID')
    const [validity, setValidity] = useState(['Hourly'])
    const [trafficLimit, setTrafficLimit] = useState([5000])
    const [checkallPackagesTime, setCheckAllPackageTimes] = useState(false)
    const [checkallPackagesTraffic, setCheckAllPackageTraffic] = useState(false)

    useEffect(() => {
        props.handleChange({ simType, validity, trafficLimit })
    }, [])

    const handleSimTypeChange = e => {
        setSimType(e)
        props.handleChange({ simType: e, validity, trafficLimit })
    }

    const handleValidityChange = e => {
        if (validity.includes(e) && validity.length > 1) {
            const filtered = validity.filter(x => x != e)
            setValidity(filtered)
            props.handleChange({ simType, validity: filtered, trafficLimit })
            return
        }
        const added = [...validity];
        !validity.includes(e) && added.push(e)
        setValidity(added)
        props.handleChange({ simType, validity: added, trafficLimit })
    }

    const handleTrafficLimitChange = e => {
        setTrafficLimit([e])
        props.handleChange({ simType, validity, trafficLimit: [e] })
    }

    return <div className="card p-3 rounded-m app-shadow mb-4 mb-lg-0">
        <div className="text-center">
            <p className="font-weight-800 text-right px-1">نوع سیمکارت : </p>
            <RoundedRadioButton
                col="6"
                colClassName="px-1"
                style={{ height: '26px', padding: '0px' }}
                options={charges}
                selected={simType}
                handleChange={handleSimTypeChange}
            />
            <div className="d-flex mt-4 px-1 mb-3">
                <p className="ml-auto font-weight-800 mb-0 font-size-15">زمان بسته : </p>
                <SelectAll
                    checked={checkallPackagesTime}
                    handleChange={(checked) => {
                        if (checked) {
                            setValidity(validities.map(x => x.id))
                            setCheckAllPackageTimes(checked)
                            props.handleChange({ simType, validity: validities.map(x => x.id), trafficLimit: trafficLimit })
                            return
                        }
                        setCheckAllPackageTimes(checked)
                        setValidity(["Hourly"])
                        props.handleChange({ simType, validity: ["Hourly"], trafficLimit: trafficLimit })
                    }}
                    className="mr-auto"
                />
            </div>
            <RoundedRadioButton
                multiple
                col="6"
                colClassName="px-1"
                style={{ height: '26px', padding: '0px' }}
                options={validities}
                selected={validity}
                handleChange={handleValidityChange}
            />
            <div className="d-flex mt-4 px-1 mb-3">
                <p className="ml-auto font-weight-800 mb-0 font-size-15">حجم بسته : </p>
                <SelectAll
                    checked={checkallPackagesTraffic}
                    handleChange={(checked) => {
                        if (checked) {
                            setCheckAllPackageTraffic(checked)
                            setTrafficLimit(traffics.map(x => x.id))
                            props.handleChange({ simType, validity: validity, trafficLimit: traffics.map(x => x.id) })
                            return
                        }
                        setCheckAllPackageTraffic(checked)
                        setTrafficLimit([500])
                        props.handleChange({ simType, validity: validity, trafficLimit: [500] })
                    }}
                    className="mr-auto"
                />
            </div>
            <RoundedRadioButton
                multiple
                checkAll={checkallPackagesTraffic}
                col="6"
                colClassName="px-1"
                fontSize="10"
                style={{ height: '26px', padding: '0px' }}
                options={traffics}
                selected={trafficLimit}
                handleChange={handleTrafficLimitChange}
            />
        </div>
    </div>
}