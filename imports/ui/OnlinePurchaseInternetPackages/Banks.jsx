import React from 'react'



export const Banks = props => {
    return <div className="online-charge-banks dark-irancell d-flex p-2 rounded align-items-center">
        <p className="ml-auto mb-0 font-size-13 font-weight-800 pr-2">انتخاب بانک</p>
        <div className="mr-auto d-flex">
            {props.options && props.options.map(x => {
                return <div
                    className={`box pointer ${props.selected == x.id ? 'active' : ''}`}
                    onClick={() => props.handleChange(x.id)}
                    key={x.id}>
                    <img src={x.image} width="25" />
                </div>
            })}
        </div>
    </div>
}