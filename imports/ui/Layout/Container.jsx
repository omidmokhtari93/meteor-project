import React from 'react'



export const Container = props => {
    return (
        <div className="container p-4">
            {props.children}
        </div>
    )
}