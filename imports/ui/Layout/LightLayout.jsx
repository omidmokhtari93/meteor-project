import React from 'react'

export const LightLayout = props => {
    return(
        <div className="light-layout">
            {props.children}
        </div>
    )
}