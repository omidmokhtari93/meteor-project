import React from 'react'
import { withRouter } from 'react-router-dom'


export const BackToPreviousPage = withRouter(props => {
    return <p className="font-size-11 text-muted pointer" onClick={() => props.history.goBack()}> <span
        className="fa fa-angle-right font-size-15 align-middle ml-1"></span> بازگشت به مرحله قبل
        </p>
})