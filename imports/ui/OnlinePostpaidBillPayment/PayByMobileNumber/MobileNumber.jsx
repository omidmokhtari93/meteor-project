import React, { useState } from 'react'
import { RoundedInput } from '../../Elements/RoundedInput'
import ReCAPTCHA from "react-google-recaptcha";
import { Button } from '../../Elements/Button'
import { withRouter } from 'react-router-dom';

export const MobileNumber = withRouter(props => {
    const [phone, setPhone] = useState('')
    const [captcha, setCaptcha] = useState(null)

    const continueOrder = () => {
        if (captcha && phone.trim() && phone.trim().length == 11) {
            props.history.push('/onlinepostpaidpayment/bymobilenumber/payment/' + phone)
        }
    }

    return <React.Fragment>
        <div className="row mt-5">
            <div className="col-12 col-md-12 col-lg-7 ml-auto mr-auto">
                <RoundedInput
                    handleChange={setPhone}
                    value={phone}
                    type="tel"
                    label="شماره تلفن همراه"
                />
            </div>
            <div className="ml-auto mr-auto mt-3">
                <ReCAPTCHA
                    sitekey="6LcVTiAaAAAAAIjSZWqr2lXSVSXUEiCPx393WX9C"
                    onChange={setCaptcha}
                    size="normal"
                    hl="fa"
                />
            </div>
            <div className="col-12 col-md-12 col-lg-7 ml-auto mr-auto mt-4">
                <Button onClick={continueOrder} className="mb-4">
                    مرحله بعد
                </Button>
            </div>
        </div>
    </React.Fragment>
})