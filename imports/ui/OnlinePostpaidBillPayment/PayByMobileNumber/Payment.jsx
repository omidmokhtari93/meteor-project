import React, { useState } from 'react'
import { BackToPreviousPage } from './BackToPreviousPage'
import { DarkInput } from '../../Elements/DarkInput'
import { withRouter } from 'react-router-dom'
import { RoundedInput } from '../../Elements/RoundedInput'
import { Banks } from '../../eCharge/Bank'
import { Button } from '../../Elements/Button'

const banks = [
    { id: 1, text: 'ملت', image: '/img/mellat.png' },
    { id: 2, text: 'توسعه تعاون', image: '/img/tosee.png' },
    { id: 3, text: 'انصار', image: '/img/ansar.png' },
    { id: 4, text: 'سینا', image: '/img/sina.png' },
    { id: 5, text: 'سینا', image: '/img/sina.png' },
]

export const Payment = withRouter(props => {
    const [amount, setAmount] = useState('')
    const [selectedBank, setSelectedBank] = useState(null)
    const payment = e => {
        console.log(e)
    }
    return <div>
        <BackToPreviousPage />
        <div className="row mt-5">
            <div className="col-12 col-md-12 col-lg-7 ml-auto mr-auto">
                <p className="mb-2 font-size-11 text-muted">شماره موبایل:</p>
                <DarkInput disabled value={props.match.params.phone} />
                <RoundedInput
                    className="mt-4"
                    label="مبلغ قابل پرداخت (ریال)"
                    value={amount}
                    handleChange={setAmount}
                    type="tel"
                    title="ریال"
                />
                <Banks
                    options={banks}
                    className="mt-4 text-center"
                    selected={selectedBank}
                    handleChange={setSelectedBank}
                />
                <Button onClick={payment} className="mb-4 mt-4">
                    پرداخت و شارژ
                </Button>
            </div>
        </div>
    </div>
})