import React, { useEffect, useState } from 'react'
import { DarkLayout } from '../Layout/DarkLayout'
import { Container } from '../Layout/Container'
import { HeaderTabs } from '../Elements/HeaderTabs'
import { PayByRefNumber } from './PayByRefNumber'
import { PayByBillPaymentId } from './PayByBillPaymentId'
import { Route, Switch, withRouter } from 'react-router-dom'
import { MobileNumber } from './PayByMobileNumber/MobileNumber'
import { Payment } from './PayByMobileNumber/Payment'

const tabs = [
    { id: 1, text: 'پرداخت با شماره تلفن همراه', path: '/onlinepostpaidpayment/bymobilenumber/getphone' },
    { id: 2, text: 'پرداخت با کد شناسه', path: '/onlinepostpaidpayment/byrefnumber' },
    { id: 3, text: 'پرداخت با شناسه قبض و پرداخت', path: '/onlinepostpaidpayment/bybillpaymentid' },
]

export const OnlinePostpaidBillPayment = withRouter(props => {
    const [selectedTab, setSelectedTab] = useState(1)
    useEffect(() => {
        const selectedTab = tabs.find(x => x.path == props.location.pathname)
        setSelectedTab(selectedTab && selectedTab.id || 1)
    }, [props.location.pathname])

    const handleChangeTabs = (id) => {
        setSelectedTab(id)
        const path = tabs.find(x => x.id == id).path
        props.history.push(path)
    }

    return <DarkLayout>
        <Container>
            <div className="row mt-lg-5 mt-0">
                <div className="col-12 col-md-12 col-lg-7 ml-auto mr-auto">
                    <HeaderTabs tabs={tabs} selected={selectedTab} handleChange={handleChangeTabs}>
                        <Switch>
                            <Route path="/onlinepostpaidpayment/bymobilenumber/getphone" render={() => <MobileNumber />} />
                            <Route path="/onlinepostpaidpayment/bymobilenumber/payment/:phone" render={() => <Payment />} />
                            <Route path="/onlinepostpaidpayment/byrefnumber" render={() => <PayByRefNumber />} />
                            <Route path="/onlinepostpaidpayment/bybillpaymentid" render={() => <PayByBillPaymentId />} />
                        </Switch>
                    </HeaderTabs>
                </div>
            </div>
        </Container>
    </DarkLayout>
})