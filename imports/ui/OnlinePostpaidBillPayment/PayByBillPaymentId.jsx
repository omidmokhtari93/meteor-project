import React, { useState } from 'react'
import ReCAPTCHA from 'react-google-recaptcha'
import { Button } from '../Elements/Button'
import { RoundedInput } from '../Elements/RoundedInput'
import { DarkInput } from '../Elements/DarkInput'


export const PayByBillPaymentId = props => {
    const [billId, setBillId] = useState('')
    const [paymentId, setPaymentId] = useState('')
    const [captcha, setCaptcha] = useState(null)

    return <div>
        <div className="row mt-5">
            <div className="col-12 col-md-12 col-lg-7 ml-auto mr-auto">
                <RoundedInput label="شناسه قبض"
                    value={billId}
                    handleChange={setBillId}
                    type="tel"
                />
                <DarkInput placeholder="شناسه پرداخت"
                    className="mt-3"
                    value={paymentId}
                    handleChange={setPaymentId}
                    type="tel"
                />
                <div className="blue-irancell p-2 font-size-13 mt-3 rounded-m">
                    برای دریافت کد شناسه باید شماره تلفن همراه خود را از طریق پیام کوتاه به شماره 2121 ارسال کنید
                </div>
            </div>
            <div className="ml-auto mr-auto mt-3">
                <ReCAPTCHA
                    sitekey="6LcVTiAaAAAAAIjSZWqr2lXSVSXUEiCPx393WX9C"
                    onChange={setCaptcha}
                    size="normal"
                    hl="fa"
                />
            </div>
            <div className="col-12 col-md-12 col-lg-7 ml-auto mr-auto">
                <Button className="mb-4 mt-4">
                    بررسی کن
                </Button>
            </div>
        </div>
    </div>
}