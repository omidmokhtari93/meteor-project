import React from 'react';
import { ProductsRoutes } from './Products/ProductsRoutes';
import { PurchaseRoute } from './PurchaseComponents/PurchaseRoutes';
import { TopNavbar } from './TopNavbar/TopNavbar';
import ReactNotification from 'react-notifications-component'
import { Route, Switch } from 'react-router-dom';
import { HomePage } from './HomePage/HomePage';
import { ECharge } from './eCharge/eCharge';
import { OnlinePostpaidBillPayment } from './OnlinePostpaidBillPayment/OnlinePostpaidBillPayment';
import { OnlinePurchaseInternetPackages } from './OnlinePurchaseInternetPackages/OnlinePurchaseInternetPackages';
import { IrancellPackagesList } from './IrancellPackagesList/IrancellPackagesList';
import { Packages } from './OnlinePurchaseInternetPackages/Packages';

export const App = () => {
  return (
    <React.Fragment>
      <ReactNotification className="sans" />
      <TopNavbar />
      <Switch>
        <Route path="/" exact render={() => <HomePage />} />
        <Route path="/echarge" render={() => <ECharge />} />
        <Route path="/packages/:type" exact render={() => <Packages />} />
        <Route path="/packages/purchase/:id" render={() => <OnlinePurchaseInternetPackages />} />
        <Route path="/onlinepostpaidpayment" render={() => <OnlinePostpaidBillPayment />} />
        <Route path="/irancellpackageslist" render={() => <IrancellPackagesList />} />
      </Switch>
      <ProductsRoutes /> {/*this is seprated because layout is diffrent from other pages */}
      <PurchaseRoute /> {/*this is seprated because layout is diffrent from other pages */}
    </React.Fragment>
  )
}

