import React from 'react'
import { Button } from '../Elements/Button'

export const PackageBox = props => {
    return <div className="rounded-m card p-4 app-shadow mb-1">
        <div className="row m-0">
            <div className="col-12 col-md-12 col-lg-1 p-0 h-100 mb-3 mb-lg-0 mr-3 mr-lg-0">
                <img src="/img/package-img.png" width="60" alt="" />
            </div>
            <div className="col-12 col-md-12 col-lg-11">
                <p className="font-weight-800 font-size-18 pr-1">بسته اینترنت همراه</p>
                <div className="row">
                    <div className="col-12 col-md-12 col-lg-8 mr-auto">
                        <p className="font-size-13 color-dark mb-lg-0">
                            شما مشترکان دائمی, پس از اتمام بسته اینترنت, می توانید ماهانه تا حجم 100 مگابایت از
                            اینترنت مصرف آزاد (با تعرفه 1/0 ریال به ازای هر کیلوبایت) استفاده کنید. پس از پایان
                            حجم مذکور, ضروریست برای دسترسی مجدد به شبکه اینترنت, نسبت به خرید اقدام کنید.
                        </p>
                    </div>
                    <div className="col-12 col-md-12 col-lg-4">
                        <Button className="p-1 font-weight-800 font-size-15" 
                        height="45px"
                        onClick={() => props.handleClick(props.id)}>مشاهده بسته ها</Button>
                    </div>
                </div>
            </div>
        </div>
    </div>
}