import React from 'react'
import { DarkLayout } from '../Layout/DarkLayout'
import { Container } from '../Layout/Container'
import { PackageBox } from './PackageBox'
import { withRouter } from 'react-router-dom'

export const IrancellPackagesList = withRouter(props => {
    return <DarkLayout>
        <Container>
            <div className="row text-right sans mt-lg-5 mt-0">
                <div className="col-12 col-md-10 col-lg-9 ml-auto mr-auto">
                    <p className="font-weight-800 font-size-17">لیست بسته های ایرانسل</p>
                    {[...Array(5)].map((x, idx) => <PackageBox key={idx}
                        handleClick={(id) => props.history.push('/packages/' + id)} />)}
                </div>
            </div>
        </Container>
    </DarkLayout>
})