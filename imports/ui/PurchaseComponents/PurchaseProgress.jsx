import React from 'react'
import { withRouter } from 'react-router-dom'

export const PurchaseProgress = withRouter(props => {
    //console.log(props)
    const route = props.location.pathname;
    return (
        <div className="purchase-progress sans noselect">
            <div className="card app-shadow">
                <ul className="list-inline ltr mb-0">
                    <li className={`list-inline-item ${route.includes('payment') ? 'active' : ''}`}>
                        بازبینی و پرداخت <i className="fa fa-credit-card"></i>
                    </li>
                    <li className={`list-inline-item ${route.includes('transport') ? 'active' : ''}`}>
                        نحوه ارسال <i className="fa fa-truck"></i>
                    </li>
                    <li className={`list-inline-item ${route.includes('simenroll') ? 'active' : ''}`}>
                        ثبت نام سیم کارت <i className="fa fa-list-alt"></i>
                    </li>
                    <li className={`list-inline-item ${route.includes('cart') ? 'active' : ''}`}>
                        سبد خرید <i className="fa fa-shopping-cart"></i>
                    </li>
                </ul>
            </div>
        </div>
    )
})