import React, { useEffect, useState } from 'react'
import { Container } from '../../Layout/Container'
import { LightLayout } from '../../Layout/LightLayout'
import { PurchaseProgress } from '../PurchaseProgress'
import { CheckBox } from '../../Elements/CheckBox'
import { Button } from '../../Elements/Button'
import { withRouter } from 'react-router-dom'
import { Modal } from '../../Elements/Modal'
import { AddOrEditAddress } from './AddOrEditAddress'
import { Address } from './Address'

export const TransportType = withRouter((props) => {
    const [recipie, setRecipie] = useState(false)
    const [deliveryAddresses, setDeliveryAddresses] = useState([])
    const [selectedAddres, setSelectedAddress] = useState({})
    const [showModal, setShowModal] = useState(false)
    const [actioneType, setActionType] = useState('')
    const [editedAddress, setEdittedAddress] = useState({})

    useEffect(() => {
        Meteor.call('user.get.address', (err, res) => {
            setDeliveryAddresses(res.addresses)
            setSelectedAddress(res.deliveryAddress)
        })
    }, [showModal])

    const updateDeliveryAddress = (selected) => {
        Meteor.call('user.update.deliveryAddres', selected, (err, res) => {
            if (res.type == 'success') {
                setSelectedAddress(selected)
            }
        })
    }
    const handleShowModal = (actioneType, address) => {
        setShowModal(!showModal)
        setActionType(actioneType)
        setEdittedAddress(address)
    }
    return (
        <LightLayout>
            <Modal
                className="p-3"
                show={showModal}
                closeModal={() => setShowModal(!showModal)}
            >
                {showModal && <AddOrEditAddress
                    type={actioneType}
                    address={editedAddress}
                />}
            </Modal>
            <Container>
                <PurchaseProgress />
                <div className="row transport sans text-right">
                    <div className="col-12 col-md-10 col-lg-9 ml-auto mr-auto">
                        <h5 className="title">تعیین نحوه ارسال</h5>
                        <div className="row m-0 receipe">
                            <div className="col-12 col-md-12 col-lg-6 mb-2 mb-lg-0">
                                <div className="label">آیا مایل به دریافت صورت حساب فیزیکی میباشید؟</div>
                            </div>
                            <div className="col-12 col-md-12 col-lg-3 mb-1 mb-lg-0">
                                <CheckBox id="no-recipie"
                                    labelClass="mr-1"
                                    title="خیر، نیازی نیست."
                                    handleChange={() => setRecipie(false)}
                                    checked={!recipie}
                                />
                            </div>
                            <div className="col-12 col-md-12 col-lg-3 mb-1 mb-lg-0">
                                <CheckBox id="recipie"
                                    labelClass="mr-1"
                                    title="بله, نیاز دارم!"
                                    handleChange={() => setRecipie(true)}
                                    checked={recipie}
                                />
                            </div>
                        </div>
                        {deliveryAddresses && deliveryAddresses.map(x => <Address
                            key={x.postalCode}
                            addressData={x}
                            selected={selectedAddres}
                            handleChange={setSelectedAddress}
                            showModal={handleShowModal}
                            updateDeliveryAddress={updateDeliveryAddress}
                            show={showModal}
                        />)}
                        <div className="card mt-4 ">
                            <div className="card-header bg-white text-right">
                                <div className="send-type">
                                    <span className="fa fa-truck"></span>
                                    شیوه ی ارسال
                                </div>
                            </div>
                            <div className="card-body send-type-comment">
                                <div className="row m-0 align-items-center h-100">
                                    <div className="col-12 col-md-8 col-lg-9 mb-3">
                                        <p>
                                            زمان تحویل سفارشات ثبت شده 7 روز کاری می باشد. لطفا کارت ملی خود را جهت احراز هویت و کپی
                                            آن را جهت ارائه به مامور پست به همراه داشته باشید. در صورت عدم ارائه ی هریک از مدارک فوق,
                                            محصول خریداری شده به شما تحویل نخواهد شد.
                                        </p>
                                    </div>
                                    <div className="col-12 col-md-4 col-lg-3 align-items-center">
                                        <div className="send-cost">
                                            هزینه ارسال : <span>رایگان</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row mt-4">
                            <div className="col-12 col-md-6 col-lg-5 ml-auto mb-3">
                                <Button onClick={() => props.history.push('/simenroll/natural/contactinfo')}
                                    color="dark">بازگشت به اطلاعات شخصی</Button>
                            </div>
                            <div className="col-12 col-md-6 col-lg-5 mr-auto mb-3">
                                <Button onClick={() => props.history.push('/payment')}>ثبت و ادامه</Button>
                            </div>
                        </div>
                    </div>
                </div>
            </Container>
        </LightLayout>
    )
})