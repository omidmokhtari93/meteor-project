import React, { useEffect, useState } from 'react'
import { Row } from '../../Elements/Row';
import { Input } from '../../Elements/Input'
import { Dropdown } from '../../Elements/Dropdown'
import { Button } from '../../Elements/Button';
import { notify } from '../../../services/notify.service'

const formIsValid = (state) => {
    const updatedState = { ...state }
    Object.keys(updatedState).map(x => {
        !updatedState[x].value
            ? (updatedState[x].touched = true)
            : (updatedState[x].touched = false)
    })
    const invalid = Object.keys(updatedState).map(x => {
        return updatedState[x].touched && !updatedState[x].value
    }).includes(true)
    return { updatedState, invalid }
}
const collectData = (state) => {
    let data = {}
    Object.keys(state).map(x => {
        data[x] = state[x].value
    })
    return data
}

const updateInputs = (inputs, newProp) => {
    Object.keys(inputs).map(x => {
        if (newProp[x]) {
            newProp[x].hasOwnProperty('value')
                ? inputs[x].value = newProp[x].value
                : inputs[x].value = newProp[x]
        }
    })
    return inputs
}
export const AddOrEditAddress = props => {
    const [inputs, setInputs] = useState({
        email: { value: '', required: false, touched: false },
        mobilePhone: { value: '', required: true, touched: false },
        phone: { value: '', required: true, touched: false },
        postalCode: { value: '', required: true, touched: false },
        city: { value: '', required: true, touched: false },
        province: { value: '', required: true, touched: false },
        address: { value: '', required: true, touched: false },
        plaque: { value: '', required: true, touched: false },
        language: { value: '', required: true, touched: false },
    })
    const [cities, setCities] = useState([])
    const [provinces, setProvince] = useState([])
    const [addressId, setAddressId] = useState('')

    useEffect(() => {
        Meteor.call('user.info.get', (err, res) => {
            const province = res.provinceAndCities.map(x => ({ value: x.name, text: x.name, cities: x.cities }))
            setProvince(province)
            if (props.type == 'edit') {
                const selectedProvince = province.find(x => x.text == props.address.province)
                setCities(selectedProvince.cities.map(x => ({ text: x.name, value: x.name })))
                setInputs(prevState => ({ ...updateInputs(prevState, props.address) }))
                setAddressId(props.address._id)
            }
        })
    }, [props.type])

    const handleChange = (name, value) => {
        let updateState = { ...inputs };
        if (name == "province") {
            const selected = provinces.find(x => x.text == value)
            setCities(selected.cities.map(x => ({ value: x.name, text: x.name })))
            updateState.city.value = ''
            updateState.city.touched = false
        }
        updateState[name].value = value;
        updateState[name].touched = true;
        setInputs(updateState)
    }

    const saveAddress = () => {
        const res = formIsValid(inputs)
        if (res.invalid) {
            notify('فیلدهای خالی را تکمیل نمایید', 'danger')
            setInputs(prevState => ({ ...updateInputs(prevState, res.updatedState) }))
        } else {
            Meteor.call('user.addOrEdit.address', props.type, {
                ...collectData(inputs),
                ...props.type == 'edit' && { _id: addressId }
            }, (err, res) => {
                if (res) {
                    notify(res.message, res.type)
                }
            })
        }

    }
    return <React.Fragment>
        <Row className="mb-0" childClass="mb-3">
            <p className="mb-1 mt-2 text-right font-weight-800 color-dark"> + افزودن آدرس جدید</p>
        </Row>
        <Row className="mb-0" childClass="mb-3">
            <Input
                type="text"
                name="email"
                label="آدرس ایمیل"
                {...inputs.email}
                handleChange={handleChange} />
            <Input
                type="number"
                label="شماره همراه"
                message="!شماره همراه خود را وارد کنید"
                name="mobilePhone"
                {...inputs.mobilePhone}
                handleChange={handleChange}
            />
        </Row>
        <Row className="mb-0" childClass="mb-3">
            <Input
                type="number"
                label="تلفن ثابت"
                name="phone"
                message="!تلفن ثابت خود را وارد کنید"
                {...inputs.phone}
                handleChange={handleChange}
            />
            <Input
                type="number"
                label="کد پستی"
                name="postalCode"
                message="!کد پستی را وارد کنید"
                {...inputs.postalCode}
                handleChange={handleChange}
            />
        </Row>
        <Row className="mb-0" childClass="mb-3">
            <Dropdown
                title="استان"
                options={provinces}
                name="province"
                message="!استان محل زندگی خود را انتخاب کنید"
                {...inputs.province}
                handleSelect={handleChange}
            />
            <Dropdown
                title="شهر"
                options={cities}
                message="!شهر محل زندگی خود را انتخاب کنید"
                handleSelect={handleChange}
                name="city"
                {...inputs.city}
            />
        </Row>
        <Row className="mb-0" childClass="mb-3">
            <Input
                type="text"
                label="آدرس کامل"
                message="!آدرس محل سکونت را وارد نمایید"
                handleChange={handleChange}
                name="address"
                {...inputs.address}
            />
        </Row>
        <Row className="mb-3" childClass="mb-3">
            <Input
                type="number"
                label="پلاک"
                name="plaque"
                message="!پلاک منزل را وارد کنید"
                {...inputs.plaque}
                handleChange={handleChange}
            />
            <Dropdown
                title="انتخاب زبان"
                options={[
                    { value: 2, text: 'فارسی' },
                    { value: 3, text: 'انگلیسی' },
                ]}
                message="!زبان خود را انتخاب نمایید"
                handleSelect={handleChange}
                name="language"
                {...inputs.language}
            />
        </Row>
        <Row>
            <Button onClick={() => saveAddress()}>{props.type == 'edit' ? 'ویرایش' : 'افزودن'}</Button>
        </Row>
    </React.Fragment>
}