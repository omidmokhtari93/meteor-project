import React from 'react'



export const Address = props => {
    const address = props.addressData;
    const isActive = address._id == props.selected._id;
    return (
        <div className="card mt-4">
            <div className="card-header bg-white">
                <div className="row">
                    <div className="col-12 col-md-5 col-lg-4 ml-auto mb-2 mb-md-0 pointer"
                        onClick={() => props.updateDeliveryAddress(address)}>
                        <div className={`recent-address ${isActive ? 'active' : ''}`}>
                            <label className={`pointer ${isActive ? 'active' : ''}`}>
                                {isActive && <span className="fa fa-check"></span>}
                            </label>
                            ارسال به آدرس فعلی
                        </div>
                    </div>
                    <div className="col-12 col-md-7 col-lg-7 mr-auto text-left">
                        <div className="row">
                            <div className="col-6 col-md-6 col-lg-4 mr-auto">
                                <button className="btn w-100 btn-outline-primary"
                                    onClick={() => props.showModal('edit', address)}>ویرایش آدرس</button>
                            </div>
                            <div className="col-6 col-md-6 col-lg-5">
                                <button className="btn w-100 btn-primary"
                                    onClick={() => props.showModal('add', null)}> + افزودن آدرس جدید</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="card-body address-info">
                <div className="row m-0">
                    <div className="col-12 col-lg-4">
                        <p className="box"><span>کد پستی : </span>{address.postalCode}</p>
                    </div>
                    <div className="col-12 col-lg-4">
                        <p className="box"><span>استان : </span>{address.province}</p>
                    </div>
                    <div className="col-12 col-lg-4">
                        <p className="box"><span>شهرستان : </span>{address.city}</p>
                    </div>
                    <div className="col-12 col-lg-12">
                        <p className="box mb-0"><span>آدرس : </span>
                            {address.address + ' پلاک ' + address.plaque}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    )
}