import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { Cart } from './Cart/Cart'
import { Payment } from './Payment/Payment'
import { PurchaseProgress } from './PurchaseProgress'
import { SimcardEnrollment } from './SimcardEnrollment/SimcrdEnreollment'
import { TransportType } from './TransportType/TransportType'


export const PurchaseRoute = props => {
    return <React.Fragment>
        <Switch>
            <Route path="/cart" render={() => <Cart />} />
            <Route path="/simenroll" render={() => <SimcardEnrollment />} />
            <Route path="/transport" render={() => <TransportType />} />
            <Route path="/payment" render={() => <Payment />} />
        </Switch>
    </React.Fragment>
}