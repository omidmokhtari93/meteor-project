import React, { useEffect, useState } from 'react'
import { LightLayout } from '../../Layout/LightLayout'
import { Container } from '../../Layout/Container'
import { PurchaseProgress } from '../PurchaseProgress'
import { Button } from '../../Elements/Button'
import { useTracker } from 'meteor/react-meteor-data'
import { numberWithCommas } from '../../../services/comma-seprator'
import { withRouter } from 'react-router-dom'
import { notify } from '../../../services/notify.service'
export const Payment = withRouter(props => {
    const [paymentMethod, setPaymentMethod] = useState('credit')
    const [userCredit, setUSerCredit] = useState(0)
    const [cartTotal, setCartTotal] = useState(0)
    useEffect(() => {
        Meteor.call('user.getCredit.cartTotal', (err, res) => {
            setUSerCredit(res.credit)
            setCartTotal(res.cartTotal)
        })
    }, [])

    const pay = () => {
        Meteor.call('payment', paymentMethod, (err, res) => {
            notify(res.message, res.type)
            props.history.push('/')
        })
    }

    return (
        <LightLayout>
            <Container>
                <PurchaseProgress />
                <div className="payment sans">
                    <div className="col-12 col-md-10 col-lg-9 ml-auto mr-auto">
                        <h5 className="text-right font-weight-800 color-dark font-size-16 mb-4">بازبینی و پرداخت</h5>
                        <div className="card py-2 px-2 rounded-5px border-15px mb-4">
                            <div className="row">
                                <div className="col-12 col-md-6 col-lg-6 text-right mb-2 mb-md-0">
                                    <span className="fa fa-shopping-cart"></span>
                                    <span className="title">بازبینی سفارش</span>
                                </div>
                                <div className="col-12 col-md-6 col-lg-6">
                                    <button className="btn btn-primary font-size-13">
                                        مشاهده سفارش
                                        <span className="fa fa-caret-down align-middle mr-3 pt-1"></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="card rounded-5px border-15px mb-4">
                            <div className="card-header bg-white text-right py-2 px-2">
                                <span className="fa fa-credit-card"></span>
                                <span className="title">نحوه پرداخت</span>
                            </div>
                            <div className="card-body type">
                                <div className={`pointer card bg-white py-2 px-2 mb-3`}
                                    onClick={() => setPaymentMethod('credit')}>
                                    <div className="row align-items-center h-100">
                                        <div className={`col-12 col-md-6 col-lg-6 ml-auto text-right mb-3 mb-md-0 
                                                ${paymentMethod == 'credit' ? 'active' : ''}`}>
                                            <label className={`check ${paymentMethod == 'credit' ? 'active' : ''}`}>
                                                {paymentMethod == 'credit' && <span className="fa fa-check"></span>}
                                            </label>
                                            <span className="pay-by-credit">پرداخت از طریق اعتبار</span>
                                        </div>
                                        <div className="col-12 col-md-6 col-lg-6 mr-auto text-left">
                                            <div className="box">
                                                اعتبار شما : <span>{numberWithCommas(userCredit)} تومان</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className={`pointer card bg-white py-2 px-2 mb-3`}
                                    onClick={() => setPaymentMethod('credit-card')}>
                                    <div className="row align-items-center h-100">
                                        <div className={`col-12 col-md-5 col-lg-5 ml-auto text-right mb-3 mb-md-0 
                                                ${paymentMethod == 'credit-card' ? 'active' : ''}`}>
                                            <label className={`check ${paymentMethod == 'credit-card' ? 'active' : ''}`}>
                                                {paymentMethod == 'credit-card' && <span className="fa fa-check"></span>}
                                            </label>
                                            <span className="pay-by-credit-card">پرداخت اینترنتی</span>
                                        </div>
                                        <div className="col-12 col-md-7 col-lg-7 mr-auto text-left">
                                            <label className="mellat-logo">
                                                <img src="/img/mellat.ico" alt="" />
                                            </label>
                                            <div className="box">
                                                مبلغ پرداختی : <span>{numberWithCommas(cartTotal)} تومان</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-12 col-md-7 col-lg-5 mr-auto mt-3">
                                        <div className="discount-box d-flex">
                                            <input placeholder="اگر کد تخفیف دارید اینجا وارد نمایید" type="text" />
                                            <button>اعمال تخفیف</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row mt-4">
                            <div className="col-12 col-md-6 col-lg-5 ml-auto mb-3">
                                <Button onClick={() => props.history.push('/cart')}
                                    color="dark">بازبینی سبد خرید</Button>
                            </div>
                            <div className="col-12 col-md-6 col-lg-5 mr-auto mb-3">
                                <Button onClick={() => pay()}>پرداخت و خرید</Button>
                            </div>
                        </div>
                    </div>
                </div>
            </Container>
        </LightLayout>
    )
})