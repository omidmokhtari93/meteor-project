import React, { useState } from 'react'
import { Route, Switch, withRouter } from 'react-router-dom'
import { PersonalInfo } from './NaturalEntityComponents/PersonalInfo'
import { ContactInfo } from './NaturalEntityComponents/ContactInfo'

export const NaturalEntity = withRouter(props => {
    const [tab, setTab] = useState('personal-info')
    return (
        <React.Fragment>
            <Switch>
                <Route path="/simenroll/natural/personalinfo" render={() => <PersonalInfo />} />
                <Route path="/simenroll/natural/contactinfo" render={() => <ContactInfo />} />
            </Switch>
        </React.Fragment>
    )
})