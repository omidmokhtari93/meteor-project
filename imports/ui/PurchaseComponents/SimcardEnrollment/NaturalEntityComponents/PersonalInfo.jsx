import React, { useEffect, useState } from 'react'
import { withRouter } from 'react-router-dom'
import { Button } from '../../../Elements/Button'
import { DatePickerContainer } from '../../../Elements/DatePicker'
import { Dropdown } from '../../../Elements/Dropdown'
import { Input } from '../../../Elements/Input'
import { RadioButton } from '../../../Elements/RadioButton'
import { Row } from '../../../Elements/Row'
import { notify } from '../../../../services/notify.service'

const options = [
    { value: 1, text: 'ایرانی' },
    { value: 2, text: 'غیر ایرانی' },
]

const genderModel = [
    { value: 1, text: 'مرد' },
    { value: 2, text: 'زن' },
]

const formIsValid = (state) => {
    const updatedState = { ...state }
    Object.keys(updatedState).map(x => {
        !updatedState[x].value
            ? (updatedState[x].touched = true)
            : (updatedState[x].touched = false)
    })
    const invalid = Object.keys(updatedState).map(x => {
        return updatedState[x].touched && !updatedState[x].value
    }).includes(true)
    return { updatedState, invalid }
}

const collectData = (state) => {
    let data = {}
    Object.keys(state).map(x => {
        data[x] = state[x].value
    })
    return data
}

const updateInputs = (inputs, newProp) => {
    inputs.name.value = newProp.name
    inputs.surName.value = newProp.surName
    inputs.father.value = newProp.father
    inputs.birth.value = newProp.birth
    inputs.nationality.value = newProp.nationality
    inputs.gender.value = newProp.gender
    inputs.nationalCode.value = newProp.nationalCode
    inputs.identity.value = newProp.identity
    return inputs
}

export const PersonalInfo = withRouter(props => {
    const [inputs, setInputs] = useState({
        name: { value: '', required: true, touched: false },
        surName: { value: '', required: true, touched: false },
        father: { value: '', required: true, touched: false },
        birth: { value: '', required: true, touched: false },
        nationality: { value: '', required: true, touched: false },
        gender: { value: '', required: true, touched: false },
        nationalCode: { value: '', required: true, touched: false },
        identity: { value: '', required: true, touched: false },
    })
    const [personalData, setPersonalData] = useState([])
    const [selectedData, setSelectedData] = useState(null)

    useEffect(() => {
        Meteor.call('user.info.get', (err, res) => {
            const firstData = res.userData[0]
            if (firstData) {
                //const updatedInputs = updateInputs(inputs, firstData)
                //setInputs(updatedInputs)
                setPersonalData(res.userData)
            }
        })
    }, [])

    const handleChange = (name, value) => {
        const updatedInputs = { ...inputs }
        updatedInputs[name].value = value
        updatedInputs[name].touched = true
        setInputs(updatedInputs)
    }

    const saveInformation = () => {
        const res = formIsValid(inputs)
        //console.log(res, inputs)
        if (res.invalid) {
            notify('فیلدهای خالی را تکمیل نمایید', 'danger')
            setInputs(res.updatedState)
        } else {
            Meteor.call('user.personalinfo.save', collectData(inputs), (err, res) => {
                props.history.push('/simenroll/natural/contactinfo')
            })
        }
    }

    const changeSelectedData = (name, id) => {
        const selected = personalData.find(x => x._id == id);
        setInputs(prevState => ({ ...updateInputs(prevState, selected) }))
        setSelectedData(selected._id)
    }

    const personalDataOptions = personalData.map(x => {
        return { value: x._id, text: x.name + ' ' + x.surName }
    })
    //console.log(inputs)

    return <React.Fragment>
        <Row className="mb-0" childClass="mb-3">
            <p className="mb-1 mt-2 text-right font-weight-800 color-dark">اطلاعات شخصی</p>
            <Dropdown
                name="userdata"
                color="blue"
                value={selectedData}
                title="استفاده از طلاعات کاربری (انتخاب کنید)"
                options={personalDataOptions}
                handleSelect={changeSelectedData}
            />
        </Row>
        <Row className="mb-0" childClass="mb-3">
            <Input type="text" label="نام"
                name="name"
                message="!نام خود را وارد کنید"
                handleChange={handleChange}
                {...inputs.name}
            />
            <Input type="text" label="نام خانوادگی"
                message="!نام خانوادگی را وارد کنید"
                name="surName"
                {...inputs.surName}
                handleChange={handleChange}
            />
        </Row>
        <Row className="mb-0" childClass="mb-3">
            <Input type="text" label="نام پدر"
                name="father"
                message="!نام پدر را وارد کنید"
                {...inputs.father}
                handleChange={handleChange}
            />
            <DatePickerContainer
                name="birth"
                message="!تاریخ تولد را انتخاب نمایید"
                label="تاریخ تولد"
                {...inputs.birth}
                handleChange={handleChange} />
        </Row>
        <Row className="mb-0" childClass="mb-3">
            <RadioButton
                message="!جنسیت را انتخاب نمایید"
                label="جنسیت"
                name="gender"
                options={genderModel}
                handleChange={handleChange}
                selected={inputs.gender.value}
                {...inputs.gender}
            />
            <Dropdown
                title="ملیت"
                options={options}
                handleSelect={handleChange}
                name="nationality"
                {...inputs.nationality}
                message="!ملیت را انتخاب نمایید"
            />
        </Row>
        <Row className="mb-3" childClass="mb-3">
            <Input
                type="number"
                label="شماره شناسنامه"
                message="!شماره شناسنامه را وارد نمایید"
                name="identity"
                {...inputs.identity}
                handleChange={handleChange}
            />
            <Input
                type="number"
                label="کد ملی"
                message="!کد ملی را وارد نمایید"
                name="nationalCode"
                {...inputs.nationalCode}
                handleChange={handleChange} />
        </Row>
        <Row className="mb-0" childClass="mb-3">
            <Button onClick={() => props.history.push('/cart')} color="dark">بازگشت به لیست محصولات</Button>
            <Button onClick={() => saveInformation()}>ثبت و ادامه</Button>
        </Row>
    </React.Fragment>
})