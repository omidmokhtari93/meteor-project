import React, { useEffect, useState } from 'react'
import { withRouter } from 'react-router-dom'
import { notify } from '../../../../services/notify.service'
import { Button } from '../../../Elements/Button'
import { Dropdown } from '../../../Elements/Dropdown'
import { Input } from '../../../Elements/Input'
import { Row } from '../../../Elements/Row'
import { CheckBox } from '../../../Elements/CheckBox'

const languageModel = [
    { value: 2, text: 'فارسی' },
    { value: 3, text: 'انگلیسی' },
]
const formIsValid = (state) => {
    const updatedState = { ...state }
    Object.keys(updatedState).map(x => {
        !updatedState[x].value
            ? (updatedState[x].touched = true)
            : (updatedState[x].touched = false)
    })
    const invalid = Object.keys(updatedState).map(x => {
        return updatedState[x].touched && !updatedState[x].value
    }).includes(true)
    return { updatedState, invalid }
}
const collectData = (state) => {
    let data = {}
    Object.keys(state).map(x => {
        data[x] = state[x].value
    })
    return data
}

const updateInputs = (inputs, newProp) => {
    Object.keys(inputs).map(x => {
        if (newProp[x]) {
            newProp[x].hasOwnProperty('value')
                ? inputs[x].value = newProp[x].value
                : inputs[x].value = newProp[x]
        }
    })
    return inputs
}
export const ContactInfo = withRouter(props => {
    const [inputs, setInputs] = useState({
        email: { value: '', required: false, touched: false },
        mobilePhone: { value: '', required: true, touched: false },
        phone: { value: '', required: true, touched: false },
        postalCode: { value: '', required: true, touched: false },
        city: { value: '', required: true, touched: false },
        province: { value: '', required: true, touched: false },
        address: { value: '', required: true, touched: false },
        plaque: { value: '', required: true, touched: false },
        language: { value: '', required: true, touched: false },
    })
    const [cities, setCities] = useState([])
    const [provinces, setProvince] = useState([])
    const [personalData, setPersonalData] = useState([])
    const [selectedData, setSelectedData] = useState(null)
    const [news, setNews] = useState(false)

    useEffect(() => {
        Meteor.call('user.info.get', (err, res) => {
            const firstData = res.userData[0]
            const province = res.provinceAndCities.map(x => ({ value: x.name, text: x.name, cities: x.cities }))
            if (firstData) {
                setPersonalData(res.userData)
                setProvince(province)
            }
        })
    }, [])

    const handleChange = (name, value) => {
        let updateState = { ...inputs };
        if (name == "province") {
            const selected = provinces.find(x => x.text == value)
            setCities(selected.cities.map(x => ({ value: x.name, text: x.name })))
            updateState.city.value = ''
            updateState.city.touched = false
        }
        updateState[name].value = value;
        updateState[name].touched = true;
        setInputs(updateState)
    }

    const saveContactInfo = () => {
        const res = formIsValid(inputs)
        if (res.invalid) {
            notify('فیلدهای خالی را تکمیل نمایید', 'danger')
            setInputs(prevState => ({ ...updateInputs(prevState, res.updatedState) }))
        } else {
            Meteor.call('user.contactinfo.save', collectData(inputs), (err, res) => {
                props.history.push('/transport')
            })
        }
    }

    const changeSelectedData = (name, id) => {
        const selected = personalData.find(x => x._id == id);
        const selectedProvince = selected.province ? provinces.find(x => x.text == selected.province) : {}
        setCities(selectedProvince.value ? selectedProvince.cities.map(x => ({ text: x.name, value: x.name })) : [])
        setInputs(prevState => ({ ...updateInputs(prevState, selected) }))
        setSelectedData(selected._id)
    }

    const personalDataOptions = personalData.map(x => {
        return { value: x._id, text: x.name + ' ' + x.surName }
    })

    //console.log(inputs)
    return <React.Fragment>
        <Row className="mb-0" childClass="mb-3">
            <p className="mb-1 mt-2 text-right font-weight-800 color-dark">اطلاعات تماس و آدرس</p>
            <Dropdown
                color="blue"
                name="userdata"
                value={selectedData}
                title="استفاده از طلاعات کاربری (انتخاب کنید)"
                options={personalDataOptions}
                handleSelect={changeSelectedData}
            />
        </Row>
        <Row className="mb-0" childClass="mb-3">
            <Input
                type="text"
                name="email"
                label="آدرس ایمیل"
                {...inputs.email}
                handleChange={handleChange} />
            <Input
                type="number"
                label="شماره همراه"
                message="!شماره همراه خود را وارد کنید"
                name="mobilePhone"
                {...inputs.mobilePhone}
                handleChange={handleChange}
            />
        </Row>
        <Row className="mb-0" childClass="mb-3">
            <Input
                type="number"
                label="تلفن ثابت"
                name="phone"
                message="!تلفن ثابت خود را وارد کنید"
                {...inputs.phone}
                handleChange={handleChange}
            />
            <Input
                type="number"
                label="کد پستی"
                name="postalCode"
                message="!کد پستی را وارد کنید"
                {...inputs.postalCode}
                handleChange={handleChange}
            />
        </Row>
        <Row className="mb-0" childClass="mb-3">
            <Dropdown
                title="استان"
                options={provinces}
                name="province"
                message="!استان محل زندگی خود را انتخاب کنید"
                {...inputs.province}
                handleSelect={handleChange}
            />
            <Dropdown
                title="شهر"
                options={cities}
                message="!شهر محل زندگی خود را انتخاب کنید"
                handleSelect={handleChange}
                name="city"
                {...inputs.city}
            />
        </Row>
        <Row className="mb-0" childClass="mb-3">
            <Input
                type="text"
                label="آدرس کامل"
                message="!آدرس محل سکونت را وارد نمایید"
                handleChange={handleChange}
                name="address"
                {...inputs.address}
            />
        </Row>
        <Row className="mb-3" childClass="mb-3">
            <Input
                type="number"
                label="پلاک"
                name="plaque"
                message="!پلاک منزل را وارد کنید"
                {...inputs.plaque}
                handleChange={handleChange}
            />
            <Dropdown
                title="انتخاب زبان"
                options={languageModel}
                message="!زبان خود را انتخاب نمایید"
                handleSelect={handleChange}
                name="language"
                {...inputs.language}
            />
        </Row>
        <div className="text-right sans mb-4">
            <CheckBox id="sms" checked={news}
                title="مایلم اطلاعات و اخبار جدید را با پیامک دریافت کنم!"
                handleChange={() => setNews(!news)}
                labelClass="font-size-13"
            />
        </div>
        <div className="contact-info comment sans">
            <p>هر شخص حقیقی می تواند حداکثر تا 10 سیم کارت را با یک کد ملی ثبت نام و فعال کند و خریداران ملزم به رعایت این تعداد هستند.</p>
            <p>خریدار می تواند با مسئولیت خودش اطلاعات یک شخص ثالث را برای واگذاری سیم کارت خریداری شده در فرم وارد کند.
            البته هنگام تکمیل فرآیند ثبت نام و فعال سازی سیم کارت خریداری شده, حضور شخص ثالث الزامی است.
        </p>
        </div>
        <Row className="mb-0" childClass="mb-3">
            <Button onClick={() => props.history.push('/simenroll/natural/personalinfo')}
                color="dark">بازگشت به اطلاعات شخصی</Button>
            <Button onClick={() => saveContactInfo()}>ثبت و ادامه</Button>
        </Row>
    </React.Fragment>
})