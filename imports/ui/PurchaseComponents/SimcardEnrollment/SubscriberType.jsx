import React from 'react'
import { withRouter } from 'react-router-dom'



export const SubscriberType = withRouter(props => {
    return (
        <div className="row mb-3 noselect">
            <div className="col-12 col-md-12 col-lg-6 ml-auto mr-auto subscriber-type sans text-center">
                <div className="list-inline mb-0 p-0">
                    <div className={`app-shadow list-inline-item m-0 pointer 
                    ${props.location.pathname.includes('/simenroll/natural') ? 'active' : ''}`}
                        onClick={() => props.history.push('/simenroll/natural/personalinfo')}>مشترکین حقیقی</div>
                    <div className={`app-shadow list-inline-item m-0 pointer 
                    ${props.location.pathname.includes('/simenroll/legal') ? 'active' : ''}`}
                        onClick={() => props.history.push('/simenroll/legal')}>مشترکین حقوقی</div>
                </div>
            </div>
        </div >
    )
})