import React, { useState } from 'react'
import { Route, Switch } from 'react-router-dom'
import { Container } from '../../Layout/Container'
import { LightLayout } from '../../Layout/LightLayout'
import { PurchaseProgress } from '../PurchaseProgress'
import { LegalEntity } from './LegalEntity'
import { NaturalEntity } from './NaturalEntity'
import { SubscriberType } from './SubscriberType'

export const SimcardEnrollment = props => {
    return (
        <LightLayout>
            <Container>
                <PurchaseProgress />
                <div>
                    <SubscriberType />
                    <div className="row">
                        <div className="col-12 col-md-12 col-lg-9 ml-auto mr-auto">
                            <div className="p-4 rounded enroll-sim dark-irancell">
                                <Switch>
                                    <Route path="/simenroll/natural" render={() => <NaturalEntity />} />
                                    <Route path="/simenroll/legal" render={() => <LegalEntity />} />
                                </Switch>
                            </div>
                        </div>
                    </div>
                </div>
            </Container>
        </LightLayout>
    )
}