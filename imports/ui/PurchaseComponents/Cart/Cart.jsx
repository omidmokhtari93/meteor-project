import React, { useEffect, useState } from 'react'
import { PurchaseProgress } from '../PurchaseProgress'
import { LightLayout } from '../../Layout/LightLayout'
import { Container } from '../../Layout/Container'
import { TotalPrice } from './TotalPrice'
import { CartItems } from './CartItems'
import { cart } from '../../../services/cart.service'
import { isMobile } from 'react-device-detect'
import { ReponsiveCartItems } from './ResponsiveCartItems'
import { useTracker } from 'meteor/react-meteor-data'
import { UserCartCollection } from '../../../methods/links'

export const Cart = props => {
    let [update, setUpdate] = useState(0);
    const user = useTracker(() => Meteor.user())
    const cartItems = useTracker(() => {
        const handler = Meteor.subscribe('cart.get')
        if (handler.ready()) {
            return UserCartCollection.findOne({})
        }
        return cart.get()
    })
    //console.log(cartItems)
    const handleChangeQuantity = (productId, packageId, value) => {
        if (user) {
            Meteor.call('cart.quantity', productId, packageId, value)
        } else {
            cart.updateQuantity(productId, packageId, value);
            setUpdate(prevState => prevState + 1)
        }
    }
    const handleDelete = (productId, packageId) => {
        if (user) {
            Meteor.call('cart.removeOneItem', productId, packageId, (err, res) => {
            })
        } else {
            cart.removeOneItem(productId, packageId);
            setUpdate(prevState => prevState + 1)
        }
    }

    const childProps = {
        products: cartItems ? cartItems.products : [],
        handleChangeQuantity: handleChangeQuantity,
        handleDelete: handleDelete
    }
    //console.log(cartItems)
    return (
        <LightLayout>
            <Container>
                <PurchaseProgress />
                <div className="cart sans">
                    <div className="row">
                        <div className="col-12 col-md-12 col-lg-10 ml-auto mr-auto">
                            <div className="text text-right">
                                مشترکین عزیز با توجه به اجباری شدن رمز پویا از ابتدای دی ماه لطفا به منظور فعال سازی رمز یکبار مصرف کارت بانکی و تایید
                                شماره تلفن همراه خود , به سایت یا شعب بانک صادر کننده کارت خود مراجعه فرمایید.
                                <a href="#" className="mr-1">اطلاعات بیشتر</a>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-8 cart-items">
                            {cartItems && cartItems.products.length
                                ? (isMobile ? <ReponsiveCartItems {...childProps} /> : <CartItems {...childProps} />)
                                : <p className="text-center ltr">!!سبد خرید شما خالی است</p>}
                        </div>
                        <div className="col-lg-4">
                            <TotalPrice {...cartItems} />
                        </div>
                    </div>
                </div>
            </Container>
        </LightLayout>
    )
}