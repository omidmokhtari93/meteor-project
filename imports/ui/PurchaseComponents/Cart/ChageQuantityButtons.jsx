import React from 'react'



export const ChangeQuantityButtons = props => {
    return (
        <React.Fragment>
            <div className="count-button"
                onClick={() => props.handleChangeQuantity(1)}>
                <span className="fa fa-plus"></span>
            </div>
            <span className="rounded-circle mx-2 noselect">{props.quantity}</span>
            <div className="count-button"
                onClick={() => props.handleChangeQuantity(-1)}>
                <span className="fa fa-minus"></span>
            </div>
        </React.Fragment>
    )
}