import React from 'react'
import { numberWithCommas } from '../../../services/comma-seprator'
import { ChangeQuantityButtons } from './ChageQuantityButtons'



export const ReponsiveCartItems = props => {
    //console.log(props)
    return (
        <React.Fragment>
            <p className="text-right font-weight-800 font-size-12">لیست محصولات</p>
            {props.products && props.products.map((pr, idx) => {
                return <div className="card responsive-cart mb-2" key={idx}>
                    <div className="row rtl m-0">
                        <div className="col-4 text-center">
                            <img alt="" src={pr.image} className="img-fluid" width="70" />
                            <div className="pt-4 pb-2">
                                <ChangeQuantityButtons
                                    handleChangeQuantity={(value) => props.handleChangeQuantity(pr.productId, pr.package._id, value)}
                                    quantity={pr.quantity}
                                />
                            </div>
                        </div>
                        <div className="col-8 text-right border-right">
                            <p className="mb-1 title">{pr.title}</p>
                            <p className="mb-0 package">
                                {pr.package.type == 'prepaid' ? ' سیم کارت دائمی ' : ' سیم کارت اعتباری '}
                                      +
                                {pr.package.name} </p>
                            <p className="price">
                                قیمت هر عدد : {numberWithCommas(pr.price)} تومان
                            </p>
                            <label className="remove"
                                onClick={() => props.handleDelete(pr.productId, pr.package._id)}>
                                ✕
                            </label>
                        </div>
                    </div>
                </div>
            })}
        </React.Fragment>
    )
}