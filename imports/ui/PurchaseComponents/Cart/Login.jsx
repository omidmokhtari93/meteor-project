import React, { useState } from 'react'
import { Input } from '../../Elements/Input'
import { Meteor } from 'meteor/meteor';
import { Modal } from '../../Elements/Modal';
import { Tabs } from '../../Elements/Tabs'
import { Button } from '../../Elements/Button'
import { notify } from '../../../services/notify.service'
import { cart } from '../../../services/cart.service';
import { getCartCount } from '../../../store/action-creators';
import { store } from '../../../store/store';

const tabs = [
    { text: 'ورود', command: 'login' },
    { text: 'ایجاد کاربر', command: 'register' }
]

export const Login = props => {
    const [username, setUsername] = useState('')
    const [password, SetPassword] = useState('')
    const [tab, setTab] = useState('login')

    const handleChangeTabs = (command) => {
        setTab(command)
        setUsername('')
        SetPassword('')
    }

    const login = () => {
        if (username && password) {
            Meteor.loginWithPassword(username, password, (err, res) => {
                if (err) {
                    notify(err.message, 'danger')
                } else {
                    notify('وارد شدید', 'success')
                    const cartItems = cart.get()
                    cartItems && Meteor.call('cart.merge', cartItems, (err, res) => {
                        if (res) {
                            notify(res.message, res.type)
                            cart.remove()
                            store.dispatch(getCartCount())
                        }
                    })
                    props.closeModal(!props.show)
                }
            })
        }
    }

    const createAccount = () => {
        if (username && password) {
            Meteor.call('user.create', username, password, (err, res) => {
                console.log(res)
                if (res.type == 'danger') {
                    notify(res.message, res.type)
                } else {
                    notify(res.message, res.type)
                    props.closeModal(!props.show)
                }
            })
        }
    }

    //Meteor.logout()
    return <React.Fragment>
        <Modal show={props.show} closeModal={() => props.closeModal(!props.show)}>
            <Tabs tabs={tabs} selected={tab} toggleTabs={handleChangeTabs} className="bg-white" />
            {tab == 'login'
                ? <div className="p-4">
                    <p className="text-right font-weight-800 font-size-12">
                        ورود به حساب کاربری
                    </p>
                    <Input className="mb-2" label="نام کاربری" handleChange={(name, value) => setUsername(value)} value={username} />
                    <Input type="password" className="mb-3" label="رمز عبور" handleChange={(name, value) => SetPassword(value)} value={password} />
                    <Button onClick={login}>ورود</Button>
                </div>
                : <div className="p-4">
                    <p className="text-right font-weight-800 font-size-12">
                        ایجاد کاربر
                    </p>
                    <Input className="mb-2" label="نام کاربری" handleChange={(name, value) => setUsername(value)} value={username} />
                    <Input type="password" className="mb-3" label="رمز عبور" handleChange={(name, value) => SetPassword(value)} value={password} />
                    <Button onClick={createAccount}>ایجاد حساب</Button>
                </div>
            }
        </Modal>
    </React.Fragment>
}