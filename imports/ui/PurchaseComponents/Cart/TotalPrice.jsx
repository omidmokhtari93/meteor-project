import React, { useState } from 'react'
import { withRouter } from 'react-router-dom'
import { numberWithCommas } from '../../../services/comma-seprator'
import { Button } from '../../Elements/Button'
import { useTracker } from 'meteor/react-meteor-data'
import { Meteor } from 'meteor/meteor';
import { Login } from './Login'

export const TotalPrice = withRouter(props => {
    const [show, setModal] = useState(false)
    const user = useTracker(() => Meteor.user())
    const enableNextStep = props.products && props.products.length > 0

    const continueOrder = () => {
        if (user) {
            props.history.push('/simenroll/natural/personalinfo')
        } else {
            setModal(true)
        }
    }
    //console.log(props)
    return <React.Fragment>
        <Login show={show} closeModal={setModal} />
        <div className="total-price">
            <div className="d-flex justify-content-between mb-3">
                <span>مبلغ کل : </span>
                <span>{numberWithCommas(props.itemsTotal || 0)}</span>
            </div>
            <div className="d-flex justify-content-between mb-3 discount">
                <span>تخفیف : </span>
                <span>{numberWithCommas(props.discount || 0)}</span>
            </div>
            <div className="d-flex justify-content-between mb-5 text-muted">
                <span>مالیات بر ارزش افزوده : </span>
                <span>{numberWithCommas(props.tax || 0)}</span>
            </div>
            <div className="d-flex justify-content-between mb-3 total">
                <span>جمع کل : </span>
                <span>{numberWithCommas(props.total || 0)}</span>
            </div>
            <Button disabled={!enableNextStep}
                className="mt-3"
                color="yellow"
                onClick={() => continueOrder()}>
                ادامه ی خرید
            </Button>
        </div>
    </React.Fragment>
})