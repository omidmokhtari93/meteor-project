import React from 'react'
import { ChangeQuantityButtons } from './ChageQuantityButtons'



export const CartItems = props => {
    //console.log(props)
    return (
        <div>
            <table>
                <thead>
                    <tr>
                        <th>لیست محصولات</th>
                        <th>تعداد</th>
                        <th>قیمت هر عدد</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {props.products && props.products.map((pr, idx) => {
                        return <tr key={idx}>
                            <td>
                                <img src={pr.image} />
                                <div className="py-3">
                                    <p className="mb-1">{pr.title}</p>
                                    <p className="mb-0">
                                        {pr.package.type == 'prepaid' ? ' سیم کارت دائمی ' : ' سیم کارت اعتباری '}
                                      +
                                      {pr.package.name} </p>
                                </div>
                            </td>
                            <td>
                                <ChangeQuantityButtons
                                    handleChangeQuantity={(value) => props.handleChangeQuantity(pr.productId, pr.package._id, value)}
                                    quantity={pr.quantity}
                                />
                            </td>
                            <td className="price">
                                <span>{pr.price}</span>
                                <span>تومان</span>
                            </td>
                            <td>
                                <span className="remove pointer"
                                    onClick={() => props.handleDelete(pr.productId, pr.package._id)}>
                                    ✕
                                </span>
                            </td>
                        </tr>
                    })}
                </tbody>
            </table>
        </div>
    )
}