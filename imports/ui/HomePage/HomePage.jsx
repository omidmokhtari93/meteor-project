import React, { useEffect, useState } from 'react'
import { Auction } from './Auction'
import { Pills } from './Pills'
import { SmallProducts } from './SmallProducts'
import { SpecialServices } from './SpecialServices'


export const HomePage = props => {
    const [products, setProducts] = useState([])
    useEffect(() => {
        Meteor.call('products.getByCategory', 'modem', (err, res) => {
            setProducts(res)
        })
    }, [])

    return (
        <div className="home sans">
            <div className="home-banner">
                <img src="/img/home-banner-2.jpg" />
            </div>
            <Pills />
            <div className="products-area">
                <div className="container">
                    <div className="row">
                        {products.map((x, idx) => {
                            if (idx > 3) {
                                return
                            }
                            return <div className="col-12 col-md-6 col-lg-3" key={idx}>
                                <SmallProducts {...x} />
                            </div>
                        })}
                    </div>
                </div>
            </div>
            <div className="container mt-5">
                <div className="home-box rounded-xl">
                    <img src="/img/222.png" className="img-fluid" alt="" />
                </div>
                <div className="row mt-4">
                    <div className="col-6">
                        <Auction />
                    </div>
                    <div className="col-6">
                        <img src="/img/555.jpg" className="img-fluid" alt="" />
                    </div>
                </div>
                <div className="row mt-4">
                    <div className="col-6">
                        <img src="/img/444.jpg" className="img-fluid" alt="" />
                    </div>
                    <div className="col-6">
                        <img src="/img/333.jpg" className="img-fluid" alt="" />
                    </div>
                </div>
            </div>
            <div className="footer-services">
                <SpecialServices />
            </div>
        </div>

    )
}