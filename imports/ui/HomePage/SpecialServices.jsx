import React from 'react'
import { DarkInput } from '../Elements/DarkInput'
import { DarkSelect } from '../Elements/DarkSelect'
import { Button } from '../Elements/Button'
import { padStart } from 'lodash'

const options = [
    { value: 20000, text: '20,000 ریال' },
    { value: 50000, text: '50,000 ریال' },
    { value: 100000, text: '100,000 ریال' }
]

export const SpecialServices = props => {
    return <div className="container special-services">
        <h4 className="text-center font-weight-800">سرویس های ویژه ایرانسل</h4>
        <div className="row text-center mt-5">
            <div className="col-12 col-md-6 col-lg-4">
                <div className="card rounded-xl p-4 app-shadow">
                    <h5 className="font-weight-800">شارژ مستقیم</h5>
                    <DarkInput
                        height="45px"
                        className="mt-4 mb-2"
                        placeholder="شماره تلفن همراه"
                    />
                    <DarkSelect
                        options={options}
                        className="mt-3"
                        height="45px"
                    />
                    <Button
                        height="45px"
                        className=" font-size-15 mt-4"
                    >رایگان</Button>
                </div>
            </div>
            <div className="col-12 col-md-6 col-lg-4">
                <div className="card rounded-xl p-4 app-shadow">
                    <h5 className="font-weight-800">افزایش حد اعتبار</h5>
                    <DarkInput
                        height="45px"
                        className="mt-4 mb-2"
                        placeholder="شماره تلفن همراه"
                    />
                    <p className="mt-4 t text-muted font-size-13">شماره تلفن همراه دائمی خود را وارد کنید</p>
                    <Button
                        height="45px"
                        className="mt-4 font-size-15"
                    >بررسی و درخواست</Button>
                </div>
            </div>
            <div className="col-12 col-md-6 col-lg-4">
                <div className="card rounded-xl p-4 app-shadow">
                    <h5 className="font-weight-800">
                        ارتقای سیم کارت از
                         <span className="english-font ml-1 mr-1">3G</span>
                         به
                         <span className="english-font ml-1 mr-1">4G</span>
                    </h5>
                    <DarkInput
                        height="45px"
                        className="mt-4 mb-1"
                        placeholder="شماره تلفن همراه"
                    />
                    <p className="pt-3 font-size-12 text-muted">
                        سیم کارتتان را 4G کنید و از سریع ترین اینترنت
                        <br />
                        کشور لذت ببرید!
                    </p>
                    <Button
                        height="45px"
                        className="mt-3 font-size-15"
                    >بررسی و درخواست</Button>
                </div>
            </div>
        </div>
    </div>
}