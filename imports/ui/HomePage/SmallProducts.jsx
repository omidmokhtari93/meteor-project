import React from 'react'
import { withRouter } from 'react-router-dom'
import { numberWithCommas } from '../../services/comma-seprator'



export const SmallProducts = withRouter(props => {

    const discont = props.oldPrice ? Math.round((props.price - props.oldPrice) / props.price * 100) : null

    return <div className="card rounded-xl p-3 app-shadow sans text-right position-relative noselect">
        {/* {discont && <label className="discount-badge noselect">{discont} % تخفیف</label>} */}
        <img src={'/img/modem11.jpg'} alt={props.title} className="img-fluid rounded-xl" />
        <div className="pt-3">
            <h5 className="font-size-13 font-weight-800 mb-3">{props.title}</h5>
            {props.description.map(desc => <p className="font-size-10 mb-0 text-muted" key={desc}>{desc}</p>)}
            {props.available ? <h4 className="d-flex mt-4 mb-4 align-items-center h-100">
                {props.oldPrice &&
                    <span className="flex-fill w-100 font-size-12 font-weight-800 align-middle text-center line-through irancell-red">
                        {numberWithCommas(props.oldPrice)} تومان</span>}
                <span className="font-size-17 font-weight-800 flex-fill w-100 text-center">{numberWithCommas(props.price)} تومان</span>
            </h4> : <h4 className="font-size-17 text-center font-weight-800 irancell-red mt-4 mb-4">ناموجود !</h4>}
            {props.available
                ? <button className="d-block m-auto border-0 rounded-xxl py-2 px-5 
                font-weight-800 irancell-yellow-background font-size-12 pointer no-ouline"
                    onClick={() => props.history.push('/product/' + props._id)}>مشاهده جزئیات</button>
                : <button className="d-block m-auto border-0 rounded-xxl py-2 px-4
                font-weight-800 irancell-blue-background text-white font-size-12 pointer no-ouline"
                    >موجود شد به من اطلاع بده !</button>}
        </div>
    </div>
})