import React, { useEffect, useState } from 'react'
import { CountDownTimer } from '../../services/count-down-timer'


export const Auction = props => {
    const [remainingTime, setRemainigTime] = useState({ days: 0, hours: 0, minutes: 0, seconds: 0 })
    useEffect(() => {
        CountDownTimer.subscribe('02/20/2021 10:1 AM', (e) => {
            setRemainigTime(e)
        })

        return () => CountDownTimer.unsubscribe()
    }, [])

    return <div className="auction rounded-xl">
        <img className="logo" src="/img/auction-logo.png" alt="" />
        <p className="text-center mt-3 number">933-933-3333</p>
        <div className="timer">
            <div className="list-inline text-center">
                <div className="list-inline-item">
                    <label>{remainingTime.seconds}</label>
                    <small>ثانیه</small>
                </div>
                <label className="align-top mt-2">:</label>
                <div className="list-inline-item">
                    <label>{remainingTime.minutes}</label>
                    <small>دقیقه</small>
                </div>
                <label className="align-top mt-2">:</label>
                <div className="list-inline-item">
                    <label>{remainingTime.hours}</label>
                    <small>ساعت</small>
                </div>
                <label className="align-top mt-2">:</label>
                <div className="list-inline-item">
                    <label>{remainingTime.days}</label>
                    <small>روز</small>
                </div>
            </div>
        </div>
        <div className="d-flex mt-1 pl-5 mb-2 align-items-center">
            <p className="ml-auto font-weight-800 font-size-20 mb-0">بالاترین پیشنهاد</p>
            <p className="mr-auto font-size-16 text-center mb-0">
                5,250,000
                <br />
                تومان
            </p>
        </div>
        <div className="d-flex pl-5 align-items-center mb-3">
            <p className="ml-auto font-weight-800 font-size-20 mb-0">قیمت شروع</p>
            <p className="mr-auto font-size-16 text-center mb-0">
                250,000
                <br />
                تومان
            </p>
        </div>
        <div className="d-flex align-items-center pl-4">
            <button className="ml-auto border-0 rounded-xxl py-2 px-5 
                font-weight-800 irancell-white-background font-size-16 pointer no-ouline">پیشنهاد  دهید</button>
            <label className="mr-auto mb-0">
                اطلاعات بیشتر <span className="fa fa-exclamation-circle font-size-20 align-middle"></span>
            </label>
        </div>
    </div>
}