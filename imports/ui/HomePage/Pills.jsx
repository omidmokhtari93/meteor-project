import React from 'react'
import { withRouter } from 'react-router-dom'


export const Pills = withRouter(props => {
    return <div className="pill-area">
        <div className="row pills">
            <div className="col-11 col-md-10 col-lg-8 ml-auto mr-auto">
                <div className="d-flex">
                    <div className="flex-fill w-100 hover" onClick={() => props.history.push('/echarge')}>
                        <div className="pill pill-5">
                            <img src="/img/pill5.png" alt="" />
                        </div>
                        <p className="mb-0 text-center font-size-12 font-weight-800 mt-1">خرید شارژ</p>
                    </div>
                    <div className="flex-fill w-100 hover"
                        onClick={() => props.history.push('/onlinepostpaidpayment/bymobilenumber/getphone')}>
                        <div className="pill pill-4">
                            <img src="/img/pill4.png" alt="" />
                        </div>
                        <p className="mb-0 text-center font-size-12 font-weight-800 mt-1">پرداخت قبض</p>
                    </div>
                    <div className="flex-fill w-100 hover">
                        <div className="pill pill-3">
                            <img src="/img/pill3.png" alt="" />
                        </div>
                        <p className="mb-0 text-center font-size-12 font-weight-800 mt-1">افزایش حد اعتبار</p>
                    </div>
                    <div className="flex-fill w-100 hover" onClick={() => props.history.push('/irancellpackageslist')}>
                        <div className="pill pill-2">
                            <img src="/img/pill2.png" alt="" />
                        </div>
                        <p className="mb-0 text-center font-size-12 font-weight-800 mt-1">بسته های اینترنت و مکالمه</p>
                    </div>
                    <div className="flex-fill w-100 hover">
                        <div className="pill pill-1">
                            <img src="/img/pill1.png" alt="" />
                        </div>
                        <p className="mb-0 text-center font-size-12 font-weight-800 mt-1">دانلود نرم افزارهای کاربردی</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
})